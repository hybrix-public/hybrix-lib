/* storage-localForage.js library for interface - (C) 2022 Joachim de Koning
 *
 * Ephemeral local browser-based storage with meta data functionality,
 * proof-of-work retainment policies, and signature registeredOwnership.
 */

const localforage = require('localforage');
const baseCode = require('../common/basecode');
const storageCommon = require('../common/storage-common');
const STORAGECONF = require('./storage-localForage-defaults.json');
const STORAGEPREFIX = STORAGECONF.storage.storagePath;
const DOTMETA = '.meta';
const jsbinaryStorage = storageCommon.jsbinaryStorage(); // meta storage schema
const sha256Buffer = storageCommon.sha256Buffer;
const createHexBuffer = storageCommon.createHexBuffer;
const formatMetaResponse = storageCommon.formatMetaResponse;

const seek = async (data, dataCallback, errorCallback) => {
  const filePath = STORAGEPREFIX + data.key;
  localforage.getItem(filePath, function (error, buffer) {
    if (error !== null) {
      errorCallback('forage seek error: '+error);
      return;
    }
    if (buffer === null) {
      dataCallback(false);
    } else {
      dataCallback(true);
    }
    return;
  });
};

const load = async (data, dataCallback, errorCallback) => {
  const filePath = STORAGEPREFIX + data.key;
  let meta, buffer;
  localforage.getItem(filePath, function (error, buffer) {
    if (buffer === null || error !== null) {
      errorCallback('forage load: Data not found!');
      return;
    }
    localforage.getItem(filePath + DOTMETA, function (error, metaBuffer) {
      if (error !== null) {
        errorCallback('forage load: Meta-data error!');
        return;
      }
      try {
        meta = jsbinaryStorage.decode( new Buffer.from(metaBuffer) );
      } catch (e) {
        meta = storageCommon.emptyMeta();
      }
      if (Object.keys(meta.publicKey).length>0 && Object.keys(meta.signature).length>0) {
        const publicKeyNACL = meta.publicKey; // when data is owned, loaded data must be properly signed!
        const signatureNACL = meta.signature;
        const bufferNACL = nacl.from_hex( baseCode.recode('utf-8', 'hex', buffer) );
        const verified = nacl.crypto_sign_verify_detached(signatureNACL, bufferNACL, publicKeyNACL);
        if (!verified) {
          errorCallback('filebased load: Data corrupted or ownership and/or signature invalid!');
          return;
        }
      }
      let result;
      switch (data.type) {
        case 'buffer':
          result = buffer;
          break;
        case 'path':
          result = filePath; // use only relative path here!
          break;
        case 'string':
        default:
          result = buffer.toString();
      }
      const now = new Date();
      if (typeof meta === 'object' && meta.hasOwnProperty('time') && typeof meta.time === 'object') meta.time.read = now; // update meta data with last time read
      else {
        const expire = new Date(storageCommon.calculateExpiry(now,size,STORAGECONF));
        meta = storageCommon.emptyMeta();
        meta.time = {
          create: now,
          read: now,
          update: now,
          expire: expire
        }
      }
      localforage.setItem(filePath + DOTMETA, jsbinaryStorage.encode(meta), function (error, metaResult) {
        if (error !== null) {
          errorCallback('forage load: Cannot set load time in meta-data!');
        } else {
          dataCallback(result);
        }
        return;
      });
    });
  });
};

const createFile = (key, value, publicKey, signature, dataCallback, errorCallback) => {
  const filePath = STORAGEPREFIX + key;
  const hash = sha256Buffer(value);
  const isHEX = /[0-9A-Fa-f]/g;
  let publicKeyNACL, signatureNACL;
  if (publicKey) {
    if (!signature) {
      errorCallback('forage save: Create data storage expected both publicKey and signature!');
      return;
    }
    if (publicKey.match(isHEX)) publicKeyNACL = nacl.from_hex(publicKey); else {
      errorCallback('forage save: Create data storage expected public key to be in hexadecimal format!');
      return;
    }
  }
  if (signature) {
    if (!publicKeyNACL) {
      errorCallback('forage save: Create data storage expected both publicKey and signature!');
      return;
    }
    if (signature.match(isHEX)) signatureNACL = nacl.from_hex(signature); else {
      errorCallback('forage save: Create data storage expected signature to be in hexadecimal format!');
      return;
    }
  }
  if (publicKeyNACL !== undefined && signatureNACL !== undefined) {
    const buffer = nacl.from_hex( baseCode.recode('utf-8', 'hex', value) );
    const verified = nacl.crypto_sign_verify_detached(signatureNACL, buffer, publicKeyNACL);
    if (!verified) {
      errorCallback('forage save: Create data storage rejected because of bad publicKey and/or signature!');
      return;
    }
  }
  const now = new Date();
  const size = value.length;
  const expire = new Date(storageCommon.calculateExpiry(now,size,STORAGECONF));
  const zerodate = new Date(0);
  const meta = {
    id: key,
    hash,
    size,
    publicKey: createHexBuffer(publicKey),
    signature: createHexBuffer(signature),
    time: {
      create: now,
      read: zerodate,
      update: now,
      expire: expire
    },
    hops: 0
  };
  localforage.setItem(filePath, value, function (error, result) {
    if (error !== null) {
      errorCallback('forage save: cannot create data!');
      return;
    }
    localforage.setItem(filePath + DOTMETA, jsbinaryStorage.encode(meta), function (error, result) {
      if (error !== null) {
        errorCallback('forage save: cannot create meta-data!');
      } else {
        dataCallback(formatMetaResponse(meta,'create'));
      }
      return;
    });
  });
};

const updateFile = (metaBuffer, key, value, publicKey, signature, dataCallback, errorCallback) => {
  const filePath = STORAGEPREFIX + key;
  let meta;
  try {
    meta = jsbinaryStorage.decode(metaBuffer);
  } catch (e) {
    meta = storageCommon.emptyMeta();
  }
  meta.id = key;
  const registeredOwner = meta.publicKey?meta.publicKey.toString('hex'):false; // check registeredOwnership
  if (publicKey || registeredOwner) {
    let verified = true;
    const isHEX = /[0-9A-Fa-f]/g;
    let publicKeyNACL;
    if (registeredOwner && publicKey === registeredOwner) {
      if (publicKey.match(isHEX)) {
        publicKeyNACL = nacl.from_hex(publicKey);
        if (signature) {
          if (signature.match(isHEX)) {
            const signatureNACL = nacl.from_hex(signature);
            const bufferNACL = nacl.from_hex( baseCode.recode('utf-8', 'hex', value) );
            verified = nacl.crypto_sign_verify_detached(signatureNACL, bufferNACL, publicKeyNACL);
          } else {
            errorCallback('forage save: Expected signature to be in hexadecimal format!');
            return;
          }
        } else {
          errorCallback('forage save: Expected signature to be added to storage object!');
          return;
        }
      } else {
        errorCallback('forage save: Expected public key to be in hexadecimal format!');
        return;
      }
    } else if (registeredOwner) {
      errorCallback('forage save: Update data storage rejected because public key missing or mismatch!');
      return;
    }
    if (!verified) {
      errorCallback('forage save: Create data storage rejected because of bad publicKey and/or signature!');
      return;
    }
  }
  const hash = sha256Buffer(value);
  if (meta.hash && meta.hash.toString('hex') === hash.toString('hex') && publicKey === registeredOwner) {
    dataCallback(formatMetaResponse(meta,'none'));
    return;
  }
  const now = new Date();
  const size = value.length;
  const expire = new Date(storageCommon.calculateExpiry(now,size,STORAGECONF));
  const timeCreate = !isNaN(meta.time.create)&&meta.time.create>0?meta.time.create:now;
  const timeRead = meta.time.read;
  meta.time = {
    create: timeCreate,
    read: timeRead,
    update: now,
    expire: expire
  }
  meta.id = key;
  meta.hash = hash;
  meta.size = size;
  meta.publicKey = createHexBuffer(publicKey);
  meta.signature = createHexBuffer(signature);
  localforage.setItem(filePath, value, function (error, result) {
    if (error !== null) {
      errorCallback('forage save: cannot update data!');
      return;
    }
    localforage.setItem(filePath + DOTMETA, jsbinaryStorage.encode(meta), function (error, result) {
      if (error !== null) {
        errorCallback('forage save: cannot update meta-data!');
      } else {
        dataCallback(formatMetaResponse(meta,'update'));
      }
      return;
    });
  });
};

const save = async (data, dataCallback, errorCallback) => {
  const filePath = STORAGEPREFIX + data.key;
  if (!data.sizeOverride && data.value.length > STORAGECONF.storage.storageLimit) {
    errorCallback('forage save: storage limit is ' + STORAGECONF.storage.storageLimit + ' bytes!');
    return;
  } else {
    localforage.getItem(filePath + DOTMETA, function (error, metaData) {
      if (metaData === null || error !== null) {
        return createFile(data.key, data.value, data.publicKey, data.signature, dataCallback, errorCallback);
      } else {
        const metaBuffer = new Buffer.from(metaData); // for efficiency we handle metadata check and retrieval here!
        return updateFile(metaBuffer, data.key, data.value, data.publicKey, data.signature, dataCallback, errorCallback);
      }
    });
    return;
  }
};

const list = async (data, dataCallback, errorCallback) => {
  const padLength = String(STORAGECONF.storage.substorageIdLength - 1).length;
  if (data.key.length < padLength) {
    errorCallback('forage list: specify a search string of ' + (padLength + 1) + ' or more characters!');
    return;
  } else {
    const filePath = STORAGEPREFIX + data.key;
    const regExpEscape = (s) => { return s.replace(/[|\\{}()[\]^$+*.]/g, '\\$&'); }
    const wildcardToRegExp = (s) => { return new RegExp('^' + s.split(/\*+/).map(regExpEscape).join('.*').replace('?','[\w]') + '$'); }
    const pattern = wildcardToRegExp(data.key);
    let result = [];
    localforage.keys().then(function(keys) {
      for (let i = 0; i < keys.length; i++) {
        const indexKey = keys[i].substr(STORAGEPREFIX.length);
        if (indexKey.substr(-DOTMETA.length) !== DOTMETA && pattern.test(indexKey)) {
          result.push(indexKey);
        }
      }
      dataCallback(result);
    }).catch(function(err) {
      errorCallback(e);
    });
    return;
  }
};

const burn = async (data, dataCallback, errorCallback) => {
  const filePath = STORAGEPREFIX + data.key;
  function removeItem(filePath,meta) {
    localforage.removeItem(filePath, function (error, result) {
      if (error !== null) {
        errorCallback('forage burn: cannot delete data!');
        return;
      }
      localforage.removeItem(filePath + DOTMETA, function (error, result) {
        if (error !== null) {
          errorCallback('forage burn: cannot delete meta-data!');
          return;
        }
        dataCallback(formatMetaResponse(meta,'delete'));
        return;
      });
    });
  }
  let meta, buffer;
  localforage.getItem(filePath + DOTMETA, function (error, metaBuffer) {
    let meta;
    try {
      meta = jsbinaryStorage.decode(metaBuffer);
    } catch (e) {
      meta = storageCommon.emptyMeta();
    }
    meta.id = data.key;
    const publicKey = data.publicKey;
    const signature = data.signature;
    const registeredOwner = meta.publicKey?meta.publicKey.toString('hex'):false; // check registeredOwnership
    if (publicKey || registeredOwner) {
      let verified = true;
      const isHEX = /[0-9A-Fa-f]/g;
      let publicKeyNACL;
      if (registeredOwner && publicKey === registeredOwner) {
        if (publicKey.match(isHEX)) {
          publicKeyNACL = nacl.from_hex(publicKey);
          if (signature) {
            if (signature.match(isHEX)) {
              const signatureNACL = nacl.from_hex(signature);
              const bufferNACL = nacl.from_hex( baseCode.recode('utf-8', 'hex', data.key) );
              verified = nacl.crypto_sign_verify_detached(signatureNACL, bufferNACL, publicKeyNACL);
            } else {
              errorCallback('forage burn: Expected signature to be in hexadecimal format!');
              return;
            }
          } else {
            errorCallback('forage burn: Expected signature to be added to storage object!');
            return;
          }
        } else {
          errorCallback('forage burn: Expected public key to be in hexadecimal format!');
          return;
        }
      } else if (registeredOwner) {
        errorCallback('forage burn: Update data storage rejected because public key missing or mismatch!');
        return;
      }
      if (!verified) {
        errorCallback('forage burn: Create data storage rejected because of bad publicKey and/or signature!');
        return;
      }
    }
    localforage.getItem(filePath, function (error, fileBuffer) {
      if (fileBuffer === null || error !== null) {
        errorCallback('forage burn: Key does not exist!');
        return;
      } else removeItem(filePath,meta);
    });
  });
};

const getMeta = async ({key}, dataCallback, errorCallback) => {
  const filePath = STORAGEPREFIX + key;
  localforage.getItem(filePath + DOTMETA, function (error, metaData) {
    if (error !== null || metaData === null) {
      errorCallback('forage meta: Error -> '+error);
      return;
    }
    try {
      const meta = {id:key, ...jsbinaryStorage.decode( new Buffer.from(metaData) )};
      dataCallback(formatMetaResponse(meta,false));
    } catch(e) {
      errorCallback('forage meta: Meta data has been corrupted!');
    }
    return;
  });
};

exports.load = load;
exports.save = save;
exports.burn = burn;
exports.meta = getMeta;
exports.seek = seek;
exports.list = list;
