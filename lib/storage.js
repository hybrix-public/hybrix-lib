// (c)2022 hybrix - Joachim de Koning
// https://developer.mozilla.org/en-US/docs/Web/API/Storage

function createLocalForageWrapper () {
  const storage = require('./storage-localForage');
  return storage;
}

function createFileStorageWrapper () {
  const storage = require('../common/storage-fileBased');
  return storage;
}

function checkStorageWrapper (storage) {
  if (typeof storage !== 'object' || storage === null) return null;
  if (typeof storage.save !== 'function') return null;
  if (typeof storage.load !== 'function') return null;
  if (typeof storage.burn !== 'function') return null;
  if (typeof storage.seek !== 'function') return null;
  if (typeof storage.meta !== 'function') return null;
  if (typeof storage.list !== 'function') return null;
  return storage;
}

function initializeStorageConnector (data) {
  if (typeof data !== 'object' || data === null) data = {storage:false};
  if (!data.storage && window.Storage) return createLocalForageWrapper(); // browser-based storage (localforage)
  else if (!data.storage && !window.Storage) return createFileStorageWrapper(); // file-based storage
  // DISABLED: if (typeof data.storage === 'object' && data.storage.key && typeof global === 'object' && global.hasOwnProperty(data.storage.key) && typeof global[data.storage.key] === 'object') return createMemoryStorageWrapper(data.storage.key);
  else return checkStorageWrapper(data.storage); // use storage object given by instantiation
}

exports.initializeStorageConnector = initializeStorageConnector;
