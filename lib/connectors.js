const HTTP_SOCKET_TIMEOUT = 15000;

function fail (error, errorCallback) {
  if (DEBUG) console.error(error);
  if (typeof errorCallback === 'function') errorCallback(error);
}

function createUri (host, query) {
  if (host.endsWith('/') && query.startsWith('/')) return host + query.substr(1);
  if (!host.endsWith('/') && !query.startsWith('/')) return host + '/' + query;
  return host + query;
}

function isLocalInstance (host) {
  return (typeof process === 'object' || host === 'localhost' || host === '127.0.0.1');
}

exports.xhrSocket = async (data, host, query, dataCallback, errorCallback) => {
  const xhr = new data.connector.XMLHttpRequest();
  let method = 'GET', body = null;
  if (typeof data.data !== 'undefined') {
    if (isLocalInstance(host)) {  // FIX for not being able to POST to localhost for some reason
      query = `${query}/?data=${data.data}`;
    } else {
      method = 'POST';
      body = data.data;
    }
  }
  xhr.open(method, createUri(host, query), true); // TODO make method an option
  if (method === 'POST') {
    xhr.setRequestHeader('Content-Type','text/plain');
    xhr.setRequestHeader('Content-Length',data.data.length);
  }
  xhr.onreadystatechange = () => {
    if (xhr.readyState === 4) {
      const data = xhr.responseText;
      if (xhr.status === 200) {
        if (xhr.getResponseHeader('Content-type') !== 'application/json') dataCallback(JSON.stringify({error: 0, data}));
        else dataCallback(data);
      } else {
        if (xhr.status === 0) fail('Failed to connect to ' + host, errorCallback);
        else fail(data, errorCallback);
      }
    }
  };
  xhr.timeout = HTTP_SOCKET_TIMEOUT;
  xhr.ontimeout = error => fail('Timeout ' + error, errorCallback);
  xhr.send(body);
};

const handleResponse = (dataCallback, errorCallback) => res => {
  // TODO make method an option  (POST,PUT,GET)
  res.setEncoding('utf8');
  const rawData = [];
  res
    .on('data', chunk => rawData.push(chunk))
    .on('timeout', () => {
      res.resume();
      fail('Request timed out', errorCallback);
    })
    .on('error', error => {
      res.resume();
      fail(`Got error: ${error.message}`, errorCallback);
    })
    .on('end', () => {
      res.resume();
      const data = rawData.join('');
      if (res.statusCode < 200 || res.statusCode > 299) return errorCallback(data);
      else {
        if (res.headers['content-type'] !== 'application/json') return dataCallback(JSON.stringify({error: 0, data}));
        else return dataCallback(data);
      }
    });
};

const getResponse = (connector, host, query, data, dataCallback, errorCallback) => {
  let uri = createUri(host, query);
  if (isLocalInstance(host) && typeof data !== 'undefined') {  // from NodeJS using POST is broken, so use the /?data= hack/workaround for local instance connectivity
    uri = `${uri}/?data=${encodeURIComponent(data)}`;
    data = undefined;
  }
  if (typeof data === 'undefined') {  // GET
    connector.get(uri, handleResponse(dataCallback, errorCallback))
      .on('error', (e) => fail(`Connector GET error -> ${e.message}`, errorCallback));
  } else {                            // POST
    const options = new URL(uri);
    options.method = 'POST';
    options.headers = {'Content-type':'text/plain','Content-Length':data.length};
    const request = connector.request(options, handleResponse(dataCallback, errorCallback))
      .on('error', (e) => fail(`Connector POST error -> ${e.message}`, errorCallback));
    request.write(data);
    request.end();
  }
};

exports.httpSocket = async (data, host, query, dataCallback, errorCallback) => getResponse(data.connector.http, host, query, data.data, dataCallback, errorCallback);

exports.httpsSocket = async (data, host, query, dataCallback, errorCallback) => getResponse(data.connector.https, host, query, data.data, dataCallback, errorCallback);

exports.localSocket = async (data, host, query, dataCallback, errorCallback) => {
  const result = data.connector.local.rout(query, data.data, dataCallback);
  if (typeof result !== 'undefined') dataCallback(result);
};
