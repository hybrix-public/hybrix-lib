const DJB2 = require('../../common/crypto/hashDJB2');
const LZString = require('../../common/crypto/lz-string');
const { DEFAULT_SALT } = require('./encrypt');
const { activate } = require('../../common/index');
const SPAWN_TASK_TIMEOUT = 60000;

/**
 * Import a client module code blob.
 *
 * @category ClientModule
 * @param {object} data
 * @param {string} data.id - Id of The client code blob.
 * @param {string} data.blob - The client code blob.
 * @param {boolean} [data.check=true] - Check if there's a different version of the blob available at the host.
 * @param {boolean} [data.channel] - The channel used for the calls. 'y' for encryped, 'z' for encryped and compresses;.
 * @param {boolean} [data.host] -  The hostname for the hybrixd node.
 */

// TODO add example

exports.import = (fail, clientModules, clientModuleBlobs, hybrixdNodes, deterministicExecutable) => async function (data, dataCallback, errorCallback) {
  const addBlob = newerVersionAvailable => blob => {
    try {
      const code = LZString.decompressFromEncodedURIComponent(blob);
      const determisticModule = activate(code);
      if (determisticModule) {
        clientModuleBlobs[data.id] = blob;
        if (deterministicExecutable) { // use a seperate process to execute the deterministic code (for node bank integration)
          createVirtualModule({id: data.id, data, determisticModule, deterministicExecutable}, (determisticModuleFromVirtual) => {
            clientModules[data.id] = determisticModuleFromVirtual;
            dataCallback({ id: data.id, newerVersionAvailable, blob: newerVersionAvailable ? blob : undefined });
          }, (err) => {
            fail('Failed to activate deterministic code: '+err, errorCallback);
          });
          return;
        } else {
          clientModules[data.id] = determisticModule;
          return dataCallback({ id: data.id, newerVersionAvailable, blob: newerVersionAvailable ? blob : undefined });
        }
      } else {
        return fail('Failed to activate deterministic code: '+code?'general error':'no data', errorCallback);
      }
    } catch (err) {
      return fail(err, errorCallback);
    }
  };

  const checkHash = sourceHash => {
    const localHash = DJB2.hash(data.blob);
    if (sourceHash.hash === localHash) { // the blob matches the hash so added
      addBlob(false)(data.blob);
    } else { // the blob does not match the hash, so retrieve it and add that one
      this.rout({ query: '/s/deterministic/code/' + data.id, host: data.host, channel: data.channel }, addBlob(true), errorCallback);
    }
  };

  if (Object.keys(hybrixdNodes).length > 0 && data.check !== false) {
    this.rout({ query: '/s/deterministic/hash/' + data.id, host: data.host, channel: data.channel }, checkHash, errorCallback);
  } else {
    addBlob(false)(data.blob);
  }
};

// this creates a wrapper around a deterministic module to fully sandbox and parallelize it on a node system
const createVirtualModule = async function ({id, determisticModule, deterministicExecutable}, mainDataCallback, mainErrorCallback) {
  const util = require('util');
  const exec = util.promisify(require('child_process').exec);
  const virtualModule = {};
  try {
    for (const method in determisticModule) {
      virtualModule[method] = function (data, dataCallback, errorCallback) {
        const args = {
          id,
          method,
          data
        };
        try {
          const user_keys = {boxPk: nacl.from_hex(global.hybrixd.node.publicKey), boxSk: nacl.from_hex(global.hybrixd.node.secretKey.substr(0, 64))}; // cut off pubkey for boxSk!
          const nonce_salt = nacl.from_hex(DEFAULT_SALT);
          const crypt_utf8 = nacl.encode_utf8( JSON.stringify(args) );
          const crypt_bin = nacl.crypto_box(crypt_utf8, nonce_salt, user_keys.boxPk, user_keys.boxSk);
          const encryptedMessage = encodeURIComponent( nacl.to_hex(crypt_bin) );
          const { fork } = require('child_process');
          const childProcess = fork(deterministicExecutable,['child'],{detached:true,timeout:SPAWN_TASK_TIMEOUT});
          // DEBUG: to deep debug temporarily comment out childProcess.once('err ...
          childProcess.once('err', (err) => {
            if(errorCallback) errorCallback(err);
            else {
              console.error('[!] spawnDeterministic error:', err);
            }
          });
          childProcess.once('message', (message) => {
            const err = message.err;
            const result = message.result;
            if (err === 0) {
              if(dataCallback) dataCallback(result);
            } else {
              if(errorCallback) errorCallback(result);
              else {
                console.error('[!] spawnDeterministic error:', result);
              }
            }
          });
          childProcess.send(encryptedMessage);
        } catch (err) {
          const errorString = `Failed to encrypt spawnDeterministic payload! -> ${err}`;
          if(errorCallback) errorCallback(errorString);
          else {
            console.error('[!] spawnDeterministic error:', errorString);
          }
        }
      };
    }
    return mainDataCallback(virtualModule);
  } catch(mainError) {
    return mainErrorCallback(mainError);  
  }
}
