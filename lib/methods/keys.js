const sha256 = require('js-sha256');
const baseCode = require('../../common/basecode');
const {DEFAULT_SALT} = require('./encrypt');

/**
   * Create signing keys
   * @category Encryption
   * @param {Object} data
   * @param {String} [data.secret] - The secret key to recreate a public key from.
   * @example
   * hybrix.sequential([
   *   'keys'                                          // generate random public and secret key pair
   *   {seed:true}, 'keys'                             // generate keys derived from current user seed
   *   someString => {seed:someString}, 'keys'         // generate keys derived from seed string
   *   mySecretKey => {secretKey:mySecretKey}, 'keys'  // return keypair for mySecretKey
   * ]
   *   , onSuccess
   *   , onError
   *   , onProgress
   * );
   */
exports.keys = (user_keys, fail, hasSession) => function (data, dataCallback, errorCallback) {
  let keys = {}, publicKey, secretKey;
  if (typeof data !== 'object' || data === null) {
    keys = nacl.crypto_sign_keypair();  // create a new random keypair
    publicKey = nacl.to_hex(keys.signPk);
    secretKey = nacl.to_hex(keys.signSk);    
  } else if (typeof data === 'object' && data.hasOwnProperty('seed')) {
    let seedHash;
    if (data.seed === true) {
      if (!hasSession()) return fail('No session available or keys provided.', errorCallback);
      const seedData = nacl.to_hex(user_keys.boxSk).concat( nacl.to_hex(user_keys.boxPk) );  // derive a keypair from hashed boxSk and boxPk non-elliptic keys
      seedHash = sha256.sha256(seedData.concat(data.salt || DEFAULT_SALT));
    } else if (typeof data.seed === 'string'){
      seedHash = sha256.sha256(data.seed.concat(data.salt || DEFAULT_SALT));  // derive keypair from hash of seed data
    }
    const seedNACL = nacl.from_hex(seedHash);
    keys = nacl.crypto_sign_seed_keypair(seedNACL);
    publicKey = nacl.to_hex(keys.signPk);
    secretKey = nacl.to_hex(keys.signSk);
  } else if (typeof data === 'object' && data.hasOwnProperty('secretKey') && typeof data.secretKey === 'string' && data.secretKey.length === 128) { // TODO: regex to see if hexadecimal
    secretKey = data.secretKey.toLowerCase(); // import secretKey and get publicKey from that
    publicKey = secretKey.substr(64, 128);
  } else return fail('Expected \'secret\' property of string type with length 128.', errorCallback);
  return dataCallback({publicKey, secretKey});
};
