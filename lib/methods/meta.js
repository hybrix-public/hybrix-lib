const metaLocal = (storage, key) => ({data: {func: (_, dataCallback, errorCallback) => storage.meta({key}, dataCallback, errorCallback)}, step: 'call'});
const metaRemote = (key, data) => ({data: {host: data.host, query: '/e/storage/meta/' + encodeURIComponent(data.key), channel: data.channel, meta: data.meta}, step: 'rout'});

/**
   * Retrieve the meta data for a storage key
   * @category Storage
   * @param {Object} data
   * @param {String} data.key - The key identifier for the data.
   * @param {String} [data.host] - The host to store the data on.
   * @param {String} [data.channel] - Indicate the channel 'y' for encryption, 'z' for both encryption and compression.
   * @param {Boolean} [data.meta=false] - Indicate whether to include meta data (process information).
   * @example
   * hybrix.sequential([
   * {username: 'DUMMYDUMMYDUMMY0', password: 'DUMMYDUMMYDUMMY0'}, 'session',
   * {host: 'http://localhost:1111/'}, 'addHost',
   * {key:'Hello', value:'World!'}, 'save',
   * {key:'Hello'}, 'meta'
   * ]
   *   , onSuccess
   *   , onError
   *   , onProgress
   * );
   */
exports.meta = (storage, fail, sessionKey, hasHost) => function (data, dataCallback, errorCallback, progressCallback) {
  if (typeof dataCallback !== 'function') { dataCallback = () => {} }
  if (typeof errorCallback !== 'function') { errorCallback = (e) => { console.error('meta: error callback missing! Error: '+e); } }
  if (typeof data !== 'object') return fail('Expected an object.', errorCallback);
  else if (!data.hasOwnProperty('key') || typeof data.key !== 'string') return fail('Expected \'key\' property of string type.', errorCallback);
  else {
    if (data.sessionKey === true && sessionKey(data.key) === null) return fail('Legacy mode requires a session', errorCallback);
    if (!hasHost() && !storage) return fail('No local storage available and no hosts added', errorCallback);

    const key = data.sessionKey === true ? encodeURIComponent(sessionKey(data.key)) : encodeURIComponent(data.key);
    const steps = {};

    // TODO fail if all/any fail?
    if (data.local !== false && storage) steps.local = metaLocal(storage, key);
    if (data.remote !== false) steps.remote = metaRemote(key, data);

    return this.parallel(steps, dataCallback, errorCallback, progressCallback);
  }
};
