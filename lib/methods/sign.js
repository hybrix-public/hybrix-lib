const baseCode = require('../../common/basecode');

/**
   * Sign a message with a secret key or verify the signature for a public key.
   * @category Encryption
   * @param {Object} data
   * @param {String} data.message - The message to sign or open.
   * @param {String} [data.public] - The public key to verify.
   * @param {String} [data.secret] - The secret key to sign with.
   * @param {Boolean|String} [data.signature] - Indication to create a detached signature or a detached signature to verify.
   * @example
   * var myKeys;
   * hybrix.sequential([
   * 'keys',  // Create key pair
   * keys => { myKeys = keys; }, // Store key pair
   * myKeys => {return {message:'Hello World', secretKey:myKeys.secret}}, 'sign',                                   // Sign message with secret key, returns signature
   * input => {return {message:'Hello World', publicKey:input.myKeys.public, signature:input.mySignature}}, 'sign'  // Verify message with public key and signature
   * ]
   *   , onSuccess
   *   , onError
   *   , onProgress
   * );
   */
exports.sign = fail => function (data, dataCallback, errorCallback) {
  if (typeof data !== 'object' || data === null) return fail('Expected data to be an object');
  else if (!data.hasOwnProperty('message') || typeof data.message !== 'string') {
    return fail('Expected property \'message\' of type string!', errorCallback);
  } else if (data.hasOwnProperty('secretKey') && typeof data.secretKey === 'string' && data.secretKey.length === 128) {
    const secretKeyNACL = nacl.from_hex(data.secretKey);
    const messageNACL = nacl.from_hex( baseCode.recode('utf-8', 'hex', data.message) );
    const messageSign = nacl.crypto_sign_detached(messageNACL, secretKeyNACL);
    const messageResult = nacl.to_hex(messageSign);
    return dataCallback(messageResult);
  } else if (data.hasOwnProperty('publicKey') && data.hasOwnProperty('signature') && typeof data.publicKey === 'string' && typeof data.signature === 'string' && data.publicKey.length === 64) {
    const messageNACL = nacl.from_hex( baseCode.recode('utf-8', 'hex', data.message) );
    const signatureNACL = nacl.from_hex(data.signature);
    const publicKeyNACL = nacl.from_hex(data.publicKey);
    const verified = nacl.crypto_sign_verify_detached(signatureNACL, messageNACL, publicKeyNACL);
    return dataCallback(verified);
  } else return fail('Expected property \'secretKey\' of string type with length 128 or \'publicKey\' of length 64.', errorCallback);
};
