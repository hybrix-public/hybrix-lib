const { STATUS_PENDING} = require('./getPending');
const Decimal = require('../../common/crypto/decimal-light');
const compressUnifiedAddress = require('../../common/compress-unified-address');
Decimal.set({ precision: 64 });

/*
let isHyAsset = () => {};
*/
/* CURRENTLY DISABLED
let isHyAsset;
try {
  isHyAsset = require('./rawTransaction/hyProtocol/hyProtocol.js').isHyAsset;
} catch (e) {
  if (e instanceof Error && e.code === 'MODULE_NOT_FOUND') isHyAsset = () => false;
  else throw e;
}
*/

function updateFuelSufficiency (data, symbol, baseSymbol, baseBalance, assets) {
  const tag = data.offset ? symbol + '#' + data.offset : symbol;
  const asset = assets[tag];
  if (asset.hasOwnProperty('symbols')) { /*&& !isHyAsset(symbol)) {*/ // unified
    for (const subSymbol in asset.symbols) { // first pass to set fuelSufficiency to subAssets
      updateFuelSufficiency(data, subSymbol, baseSymbol, baseBalance, assets);
    }
    let sufficientFuel = false;
    for (const subSymbol in asset.symbols) { // second pass to determine overall fuelSufficiency
      const subTag = data.offset ? subSymbol + '#' + data.offset : subSymbol;
      const subAsset = assets[subTag];
      const subSufficientFuel = subAsset.data.sufficientFuel;
      asset.data.subSufficientFuels[subSymbol] = subSufficientFuel;
      for (const feeSymbol in subAsset.data.baseBalances) {
        asset.data.baseBalances[feeSymbol] = subAsset.data.baseBalances[feeSymbol];
      }
      if (subSufficientFuel) sufficientFuel = true;
    }
    asset.data.sufficientFuel = sufficientFuel;
  } else if (asset['fee-symbol'] === baseSymbol) { // regular or hy asset that pays fees in baseSymbol
    const fee = new Decimal(asset.fee); // TODO multi fee
    asset.data.baseBalances[asset['fee-symbol']] = baseBalance;
    asset.data.sufficientFuel = baseBalance !== 'n/a' && !fee.gt(baseBalance);
  }
}

// check with other assets have symbol as fee-symbol and update fuel sufficiency accordingly
function updateFuelSufficiencies (data, baseSymbol, baseBalance, assets) {
  for (const tag in assets) {
    const [symbol, offset] = tag.split('#');
    if ((offset || 0) === (data.offset || 0)) {
      updateFuelSufficiency(data, symbol, baseSymbol, baseBalance, assets);
    }
  }
}

/*
function updateHyBalancesSteps (data, symbol, assets) {
  const tag = data.offset ? symbol + '#' + data.offset : symbol;
  const asset = assets[tag];
  return [
    {query: '/a/' + symbol + '/subbalances/' + asset.data.address, fallback: undefined, channel: data.channel}, 'rout', // get all subBalances
    subBalances => {
      if (subBalances instanceof Array) {
        let balance = new Decimal(0);
        asset.data.subBalances = resetObject(asset.data.subBalances);
        for (const subBalance of subBalances) {
          asset.data.subBalances[subBalance.symbol + ':' + subBalance.address] = subBalance.balance;
          asset.data.lastRefreshTime = Date.now();
          balance = balance.add(new Decimal(subBalance.balance));
        }
        const formedBalance = this.form({amount: balance.toString(), factor: asset.factor});
        asset.data.balance = formedBalance;
        updateFuelSufficiencies(data, symbol, formedBalance, assets);
      }
      return asset.data.balance;
    }
  ];
}
*/

// if not object return {} else delete all values, keeping the reference intact
function resetObject (object) {
  if (typeof object !== 'object' || object === null) return {};
  for (const key in object) delete object[key];
  return object;
}

/*
function updateRegularBalanceSteps (data, symbol, assets) {
  const tag = data.offset ? symbol + '#' + data.offset : symbol;
  const asset = assets[tag];
  return [
    {query: '/a/' + symbol + '/balance/' + asset.data.address, fallback: undefined, channel: data.channel}, 'rout',
    balance => {
      const asset = assets[tag];
      if (typeof balance !== 'undefined') {
        asset.data.balance = balance;
        asset.data.lastRefreshTime = Date.now();
        asset.data.subBalances = resetObject(asset.data.subBalances);
        asset.data.subBalances[symbol + ':' + asset.data.address] = balance;
        updateFuelSufficiencies(data, symbol, balance, assets);
      }
      return asset.data.balance;
    }
  ];
}
*/

  /*
const updateBalance = (data, symbol, assets) => function (xdata, dataCallback, errorCallback, progressCallback) {
  const tag = data.offset ? symbol + '#' + data.offset : symbol;
  return assets.hasOwnProperty(tag)
    ? this.sequential(getUpdateBalanceSteps(data, symbol, assets), dataCallback, errorCallback, progressCallback)
    : errorCallback(`Asset ${symbol} could not be added.`);
};
  */

/*
function addAssetAndUpdateBalanceSteps (data, symbol, assets) {
  return [
    {symbol}, 'addAsset',
    {func: updateBalance(data, symbol, assets).bind(this)}, 'call'
  ];
}
*/

  /*
function getUpdateBalanceSteps (data, symbol, assets) {
  const tag = data.offset ? symbol + '#' + data.offset : symbol;
  if (assets.hasOwnProperty(tag)) {
    const asset = assets[tag];
    if (isHyAsset(asset)) return updateHyBalancesSteps.bind(this)(data, symbol, assets);
    else if (!asset.hasOwnProperty('symbols')) return updateRegularBalanceSteps(data, symbol, assets);
    else return []; // unified assets are skipped as they are build using their sub balances
  } else return addAssetAndUpdateBalanceSteps.bind(this)(data, symbol, assets);
}
  */

function updateUnifiedAsset (data, asset, assets) {
  let balance = new Decimal(0);
  asset.data.subBalances = resetObject(asset.data.subBalances);
  asset.data.pending.length = 0;
  for (const subSymbol in asset.symbols) {
    const subTag = data.offset ? subSymbol + '#' + data.offset : subSymbol;
    const subAsset = assets[subTag];
    const subBalance = subAsset.data.balance;
    asset.data.subBalances[subSymbol + ':' + subAsset.data.address] = subBalance;
    if (typeof subBalance !== 'undefined' && subBalance !== 'n/a') {
      const weight = asset.symbols[subSymbol];
      const weightedSubBalance = new Decimal(subBalance).times(weight);
      balance = balance.plus(weightedSubBalance);
    }
    asset.data.pending.push(...subAsset.data.pending);
  }
  const formedBalance = this.form({amount: balance.toString(), factor: asset.factor});
  updateFuelSufficiencies(data, asset.symbol, formedBalance, assets);
  asset.data.balance = formedBalance;
}

// add the required sub assets to the list of assets to refresh
function determineWhichAssetsToGet (data, assets, symbols) {
  const assetList = {};
  for (const symbol of symbols) {
    const tag = data.offset ? symbol + '#' + data.offset : symbol;
    if (!assets.hasOwnProperty(tag)) continue;
    const asset = assets[tag];
    assetList[symbol] = true;

    if (asset.hasOwnProperty('symbols')) { // unified
      for (const subSymbol in asset.symbols) {
        const subTag = data.offset ? subSymbol + '#' + data.offset : subSymbol;
        if (!assets.hasOwnProperty(subTag)) continue;
        const subAsset = assets[subTag];
        if (!assetList.hasOwnProperty(subSymbol)) assetList[subSymbol] = false; // add the subSymbols but set visibility to false
        const feeSymbol = subAsset['fee-symbol'];
        if (feeSymbol !== symbol && !assetList.hasOwnProperty(feeSymbol)) assetList[feeSymbol] = false; // add fee base sub asset
      }
    } else {
      const feeSymbol = asset['fee-symbol'];
      if (feeSymbol !== symbol && !assetList.hasOwnProperty(feeSymbol)) assetList[feeSymbol] = false; // add fee base asset
    }
  }
  return assetList;
}

function isUnified (asset) {
  return asset.hasOwnProperty('symbols'); /* && !isHyAsset(asset);*/
}

// Update the unified balances
const handleUndefinedAndUnifiedBalances = (data, assets, assetList, dataCallback, errorCallback, progressCallback) => function () {
  for (const symbol in assetList) {
    const tag = data.offset ? symbol + '#' + data.offset : symbol;
    if (assets.hasOwnProperty(tag)) {
      const asset = assets[tag];
      if (isUnified(asset)) updateUnifiedAsset.bind(this)(data, asset, assets); // Unified Assets
    }
  }

  // replace undefineds with 'n/a' and determine which assets to return (only non sub assets)
  const returnList = [];
  for (const symbol in assetList) {
    const tag = data.offset ? symbol + '#' + data.offset : symbol;
    if (assets.hasOwnProperty(tag)) {
      if (assetList[symbol]) returnList.push(symbol); // only add visible assets to return list
      if (typeof assets[tag].data.balance === 'undefined') {
        assets[tag].data.balance = 'n/a'; // if no balance is available set to n/a
      }
    }
  }

  this.asset({symbol: returnList, offset: data.offset}, dataCallback, errorCallback, progressCallback);
};

// ensure that all requested assets are added, including their subassets
const addAssets = function (data, symbols, dataCallback, errorCallback) {
  const addAssetSteps = {};
  for (const symbol of symbols) addAssetSteps[symbol] = {data: {symbol, offset: data.offset}, step: 'addAsset'};
  return this.parallel(addAssetSteps, () => dataCallback(symbols), errorCallback);
};

function getTags (pendingTransaction) {
  if (pendingTransaction.type === 'regular') {
    if (typeof pendingTransaction.meta !== 'object' || pendingTransaction.meta === null) return [];
    const tag = pendingTransaction.meta.offset ? pendingTransaction.meta.symbol + '#' + pendingTransaction.meta.offset : pendingTransaction.meta.symbol;
    return [tag];
  } else if (pendingTransaction.type === 'deal') {
    if (typeof pendingTransaction.meta !== 'object' || pendingTransaction.meta === null) return [];
    if (typeof pendingTransaction.meta.ask !== 'object' || pendingTransaction.meta.ask === null) return [];
    if (typeof pendingTransaction.meta.bid !== 'object' || pendingTransaction.meta.bid === null) return [];
    const askTag = pendingTransaction.meta.ask.symbol;
    const bidTag = pendingTransaction.meta.bid.symbol;
    return [askTag, bidTag];
  } else return [];
}

const refreshPending = (assets, pendingTransactions) => {
  const clearedTags = [];
  for (const id in pendingTransactions) {
    const pendingTransaction = pendingTransactions[id];
    if (pendingTransaction.status !== STATUS_PENDING) continue;
    const tags = getTags(pendingTransaction);
    for (const tag of tags) {
      if (!assets.hasOwnProperty(tag)) continue;
      const asset = assets[tag];
      if (isUnified(asset)) continue; // aggregation will be handled later
      if (!clearedTags.includes(tag)) {
        clearedTags.push(tag);
        asset.data.pending.length = 0;
      }
      asset.data.pending.push(id);
    }
  }
};

function decompressUnifiedAssetAddress (symbol, address) {
  let decoded = null;
  if (symbol) {
    if (address.startsWith(symbol + ':')) { // `${symbol}:${encoded}`
      decoded = compressUnifiedAddress.decode(address.split(':')[1]);
    } else if(!address.includes(':')) {
      decoded = compressUnifiedAddress.decode(address);
    } else {
      console.error('refeshAsset: Unified asset main symbol deviates from prefix symbol!');
    }
  } else {
    console.error('refreshAsset: Unified asset main symbol must be specified!');
  }
  return decoded; // `${subSymbol}:${subAddress},...`
}

// refrest all requested assets
const refreshAssets = (assets, data, pendingTransactions) => function (symbols, dataCallback, errorCallback, progressCallback) {
  const assetList = determineWhichAssetsToGet(data, assets, symbols);
  const cache = data.cache || 5000;
  const threshold = Date.now() - cache;
  const steps = [];

  let symbolPortfolio = [];
  for (const symbol in assetList) {
    const tag = data.offset ? symbol + '#' + data.offset : symbol;
    const lastRefreshTime = assets.hasOwnProperty(tag) ? assets[tag].lastRefreshTime : null;
    if (lastRefreshTime > threshold) continue; // skip update, balance data is still fresh enough
    // DEPRECATED IN FAVOUR OF PORTFOLIO ENGINE: steps[symbol] = {data: getUpdateBalanceSteps.bind(this)(data, symbol, assets), step: 'sequential'};
    if (assets.hasOwnProperty(tag)) {
      const asset = assets[tag];
      if (asset.hasOwnProperty('symbols')) {
        // decompress address and add every symbol
        const addresses = decompressUnifiedAssetAddress(symbol, assets[tag].data.address).split(',');
        for (const address of addresses) symbolPortfolio.push(address);
      } else symbolPortfolio.push(`${symbol}:${assets[tag].data.address}`);
    // addAssetPush
    // DEPRECATED: return addAssetAndUpdateBalanceSteps.bind(this)(data, symbol, assets)
    } else steps.push([{symbol},'addAsset']);
  }

  // portfolio call replaces the numerous queries previously needed
  steps.push(
    [{query: '/e/portfolio/balances/' + symbolPortfolio.join(','), fallback: undefined, channel: data.channel}, 'rout',
      balances => {
        for (const symbol in balances) {
          const tag = data.offset ? symbol + '#' + data.offset : symbol;
          const asset = assets[tag];
          if (typeof balances[symbol] !== 'undefined') {
            asset.data.balance = balances[symbol];
            asset.data.lastRefreshTime = Date.now();
            asset.data.subBalances = resetObject(asset.data.subBalances);
            asset.data.subBalances[symbol + ':' + asset.data.address] = balances[symbol];
            updateFuelSufficiencies(data, symbol, balances[symbol], assets);
          }
        }
        return balances;
      }
    ]
  );

  refreshPending(assets, pendingTransactions);

  return this.parallel(steps, handleUndefinedAndUnifiedBalances(data, assets, assetList,
    dataCallback, errorCallback, progressCallback).bind(this),
  errorCallback,
  progressCallback);
};

/**
 * Update the balance of a given asset (or all assets if no symbol is defined)
 * @category AssetManagement
 * @param {Object} data
 * @param {string} [data.symbol] - The symbol of the asset to be refreshed, leave undefined to refresh all assets.
 * @param {string} [data.cache=5000] - Asset will only be refreshed if data is older than this many milliseconds.
 * @param {string} [includeBaseAssets=false] - Include the base assets. (The main chain for tokens)
 * @param {string} [data.offset=0] - The deterministic offset
 * @example
 * hybrix.sequential([
 * {username: 'DUMMYDUMMYDUMMY0', password: 'DUMMYDUMMYDUMMY0'}, 'session',
 * {host: 'http://localhost:1111/'}, 'addHost',
 * {symbol: 'dummy'}, 'refreshAsset',
 * ]
 *   , onSuccess
 *   , onError
 *   , onProgress
 * );
 */
exports.refreshAsset = (assets, fail, hasSession, pendingTransactions) => function (data, dataCallback, errorCallback, progressCallback) {
  if (!hasSession()) return fail('No session available.', errorCallback);
  if (typeof data !== 'object' || data === null) return fail('refreshAsset: Expected data to be an object', errorCallback);

  const followUp = symbols => refreshAssets(assets, data, pendingTransactions).bind(this)(symbols, dataCallback, errorCallback, progressCallback);
  const symbols = [];
  if (!data.hasOwnProperty('symbol')) {
    if ((data.offset || 0) !== 0) fail('refreshAsset: Expected explicit symbol for offset.');
    else return followUp(Object.keys(assets)); // refresh all assets
  } else if (data.symbol instanceof Array) symbols.push(...data.symbol);
  else if (typeof data.symbol === 'string') symbols.push(data.symbol);
  else return fail('refreshAsset: Expected string or array symbol property.', errorCallback);
  return addAssets.bind(this)(data, symbols, followUp, errorCallback, progressCallback);
};
