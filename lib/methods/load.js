const GRACE_PERIOD = 60;  // any changes more than 60 seconds offset count as a candidate for synchronization

const compareMeta = async (results, dataCallback, errorCallback) => {
  let action = 'none';
  if (typeof results.local === 'object' && typeof results.remote === 'object' && results.local !== null && results.remote !== null) {
    let meta;
    const localMeta = results.local;
    const remoteMeta = results.remote;
    if (typeof localMeta.id === 'string' && typeof remoteMeta.id === 'string' && localMeta.id === remoteMeta.id) {
      if (localMeta.hash !== remoteMeta.hash) {
        const localModified = localMeta.time.update>localMeta.time.create?localMeta.time.update:localMeta.time.create
        const remoteModified = remoteMeta.time.update>remoteMeta.time.create?remoteMeta.time.update:remoteMeta.time.create
        if(localModified>remoteModified) {
          action = 'push';
          meta = localMeta;
        } else {
          action = 'pull';
          meta = remoteMeta;
        }
      } else {
          meta = localMeta;
      }
      return dataCallback({action,meta});
    } else {
      return errorCallback('load: meta IDs do not match or missing! '+JSON.stringify(results));
    }
  } else {
    let meta;
    if (typeof results.local === 'object' && results.local !== null) {
      meta = results.local;
      if (typeof meta.id === 'string') action = 'push';
      else errorCallback('load: local meta data corrupted!');
    } else if (typeof results.remote === 'object' && results.remote !== null) {
      meta = results.remote;
      if (typeof meta.id === 'string') action = 'pull';
      else errorCallback('load: remote meta data corrupted!');
    } else return errorCallback('load: could not read meta-data to compare! '+JSON.stringify(results));
    return dataCallback({action,meta});
  }
};

const handleMerge = data => function (results, dataCallback, errorCallback) {
  const saveOptions = {key: results.meta.id, signature: results.meta.signature, publicKey: results.meta.publicKey, channel: data.channel, host: data.host, sessionKey: data.sessionKey, encrypted: data.encrypted};
  const NOP = () => {};
  let value;
  if (data.hasOwnProperty('mergeStrategy') && typeof data.mergeStrategy === 'function') { // either a mergeStrategy can be used to merge data based on content, or instead synchronize according to action
    if (results.local === results.remote) return dataCallback(results.local);
    value = data.mergeStrategy(results.local, results.remote, results.action);
    if (typeof value !== 'undefined' && value !== null && value !== results.remote && data.sync !== false) this.save({value, local: false, ...saveOptions}, NOP, NOP); // sync merged value
    if (typeof value !== 'undefined' && value !== null && value !== results.local && data.sync !== false) this.save({value, remote: false, ...saveOptions}, NOP, NOP); // sync merged value
  } else {
    const returnRemoteValue = () => { dataCallback(results.remote); }
    const fallbackToLocalValue = () => {
      console.error('load: remote signing data does not match, getting data from local!');
      dataCallback(results.local);
    }
    if (results.action === 'none') {
      value = results.local;
    } else if (results.action === 'push' && typeof results.local !== 'undefined' && results.local !== null) {
      value = results.local;
      if (data.remote !== false && data.sync !== false) this.save({value, local: false, ...saveOptions}, NOP, NOP); // sync
    } else if (results.action === 'pull' && typeof results.remote !== 'undefined' && results.remote !== null) {
      value = results.remote;
      if (data.local !== false && data.sync !== false) this.save({value, remote: false, ...saveOptions}, returnRemoteValue, fallbackToLocalValue); // sync, wherein save checks signature validity
      return; // callback is handled by save function in this case
    } else {
      // local or remote data is corrupt, so fallback on available data, else burn all
      if (typeof results.local !== 'undefined' && results.local !== null) {
        // DEBUG ONLY: console.error('load: remotely corrupted data, resyncing!');
        value = results.local;
        if (data.remote !== false && data.sync !== false) this.save({value, local: false, ...saveOptions}, NOP, NOP); // sync
      } else if (typeof results.remote !== 'undefined' && results.remote !== null) {
        console.error('load: locally corrupted data, getting data from remote!');
        value = results.remote;
        if (data.local !== false && data.sync !== false) this.save({value, remote: false, ...saveOptions}, returnRemoteValue, fallbackToLocalValue); // sync, wherein save checks signature validity
        return; // callback is handled by save function in this case
      } else {
        console.error('load: locally and remotely corrupted data, clearing from cache!');
        this.burn({...saveOptions});
        return errorCallback('load: stored data corrupted, clearing from cache!');
      }
    }
  }
  dataCallback(value);
  return;
};

const decryptionSteps = encrypted => (encrypted === false
  ? [value => { return value; }]
  : [encryptedValue => { return {data: encryptedValue, defaultValue: null}; }, 'decrypt']
);

const metaLocal = (key, data, storage) => [{key}, storage.meta];

const metaRemote = (key, data) => [{host: data.host, query: '/e/storage/meta/' + encodeURIComponent(key), channel: data.channel}, 'rout'];

const loadLocal = (data, encrypted, storage) => {
  return [  // always pull in local data so signing can be checked in merge step
    {key:data.meta.id} , storage.load,
    ...decryptionSteps(encrypted)
  ];
}

const loadRemote = (data, encrypted) => {
  return data.action === 'pull'?[
    {host: data.host, query: '/e/storage/load/' + encodeURIComponent(data.meta.id), channel: data.channel}, 'rout',
    ...decryptionSteps(encrypted)
  ]:null;
}

/**
   * Retrieve value associated with key from the hybrixd node storage
   * @category Storage
   * @param {Object} data
   * @param {String} data.key - The key identifier for the data.
   * @param {Boolean} [data.encrypted=true] - whether to encrypt the data with the user key, true by default.
   * @param {Boolean} [data.sessionKey=false] - A toggle to use a sessionKey storage method.
   * @param {String} [data.fallback] - Provide a fallback value, if call fails this value is used
   * @param {String} [data.host] - The host to store the data on.
   * @param {String} [data.channel=''] - Indicate the channel 'y' for encryption, 'z' for both encryption and compression.
   * @param {Boolean} [data.local=true] - Whether to use local storage if available
   * @param {Boolean} [data.remote=true] - Whether to use remote storage
   * @param {Boolean} [data.mergeStrategy=(local,remote)=>local] - What to do if results from local and remote storage differs
   * @param {Boolean} [data.sync=true] - If only available on either remote or storage, save it to the other as well
   * @example
   * hybrix.sequential([
   * {username: 'DUMMYDUMMYDUMMY0', password: 'DUMMYDUMMYDUMMY0'}, 'session',
   * {host: 'http://localhost:1111/'}, 'addHost',
   * {key:'Hello', value:'World!'}, 'save',
   * {key:'Hello'}, 'load'
   * ]
   *   , onSuccess
   *   , onError
   *   , onProgress
   * );
   */
exports.load = (storage, fail, sessionKey, hasHost) => function (data, dataCallback, errorCallback, progressCallback) {
  if (typeof dataCallback !== 'function') { dataCallback = () => {} }
  if (typeof errorCallback !== 'function') { errorCallback = (e) => { console.error('load: error callback missing! -> '+e); } }
  if (typeof data !== 'object') return fail('Expected an object!', errorCallback);
  else if (!data.hasOwnProperty('key') || typeof data.key !== 'string') return fail('load: expected property \'key\' of type string!', errorCallback);
  else {
    const useFallbackCallbackOrError = error => {
      if (data.hasOwnProperty('fallback')) return dataCallback(data.fallback);
      else return fail(error, errorCallback);
    };
    if (!hasHost() && !storage) return fail('No local storage available and no hosts added!', errorCallback);

    let key;
    if (data.sessionKey === true) {
       const sessionEncodedKey = sessionKey(data.key)
       if (sessionEncodedKey === null) return fail('load: Session key storage mode requires a session!', errorCallback);
       else key = encodeURIComponent(sessionEncodedKey);
    } else key = encodeURIComponent(data.key);

    const metaSteps = {};
    if (data.local !== false && storage) {
      metaSteps.local = metaLocal(key, data, storage);
    }
    if (data.remote !== false) {
      metaSteps.remote = metaRemote(key, data);
    }
    
    const encrypted = data.encrypted === false?false:true;

    return this.sequential([
      metaSteps, 'parallel',
      compareMeta,
      results => {
        return {
          action: {data:results.action,step:'id'},
          meta: {data:results.meta,step:'id'},
          local: loadLocal(results, encrypted, storage),
          remote: loadRemote(results, encrypted)
        };
      }, 'parallel',
      {func: handleMerge(data).bind(this)}, 'call'
    ], dataCallback, useFallbackCallbackOrError, progressCallback);
  }
};
