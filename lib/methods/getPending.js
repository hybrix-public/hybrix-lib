const STATUS_PENDING = 0;
const STATUS_FAILED = -1;
const STATUS_COMPLETED = 1;
const PENDING_TRANSACTION_STORAGE_KEY = 'HY0038';
exports.PENDING_TRANSACTION_STORAGE_KEY = PENDING_TRANSACTION_STORAGE_KEY;
exports.STATUS_PENDING = STATUS_PENDING;
exports.mergeStrategy = mergeStrategy;

const MAX_TX_AGE = 1000 * 8 * 60 * 60 * 24;  // after 24 hours transactions are considered failed
const MIN_TX_AGE = 1000 * 16; // minimum time to wait before finalizing a transaction
const MAX_ENTRIES = 120; // maximum amount of entries that fit in a pending storage object
const MAX_SWAP_UPDATE_THRESHOLD = 1000 * 1209600; // status of a swap is no longer queried after 14 days, even if not completed

 /*

  pendingTransactions = {
    'ref=symbol:id' : { type:'regular',ref, status, timestamp, meta:{id,symbol,amount,target,source}}
  }

 */

const finalize = (pendingTransactions, data, dataCallback, hasSession) => function () {
  if (data.refresh !== false) { // fail stale transactions
    const now = Date.now();
    for (const ref in pendingTransactions) {
      const pendingTransaction = pendingTransactions[ref];
      if (pendingTransaction.status !== STATUS_COMPLETED && (now - pendingTransaction.timestamp) > MAX_TX_AGE) pendingTransaction.status = STATUS_FAILED;
    }
  }
  if (data.remove !== false) {
    for (const ref in pendingTransactions) {
      if (pendingTransactions[ref].status === STATUS_COMPLETED) delete pendingTransactions[ref];
    }
  }
  if ((data.refresh !== false || data.remove !== false) && data.sync !== false && hasSession()) { // save locally if data has been culled - this will be synced later on when a new tx is created, or on next login load
    const NOP = () => {};
    this.save({key: PENDING_TRANSACTION_STORAGE_KEY, value: pendingTransactions, remote: false, sessionKey: true}, NOP, NOP);
  }
  const result = {...pendingTransactions};
  return dataCallback(result);
};

function refreshPendingTransaction (data, pendingTransaction) {
  const type = pendingTransaction.type;
  const hostData = {channel: data.channel};
  if (data.hasOwnProperty('host')) {
    if (typeof data.host === 'string') hostData.host = data.host;
    else if (typeof data.host === 'object' && data.host !== null && data.host.hasOwnProperty(type)) {
      hostData.host = data.host[type];
    } else if (typeof data.host === 'object' && data.host !== null && data.host.hasOwnProperty('default')) {
      hostData.host = data.host.default;
    }
  }

  const now = Date.now();
  if (isNaN(pendingTransaction.progress)) pendingTransaction.progress = 0;
  if (type === 'regular' && pendingTransaction.status !== STATUS_COMPLETED && (now - pendingTransaction.timestamp) > MIN_TX_AGE) {
    const symbol = typeof pendingTransaction.meta.symbol === 'string' ? pendingTransaction.meta.symbol : pendingTransaction.ref.split(':')[0];
    const txid = typeof pendingTransaction.meta.id === 'string'
                   ? pendingTransaction.meta.id
                   : pendingTransaction.meta.id instanceof Array
                     ? pendingTransaction.meta.id.join(',')
                     : pendingTransaction.ref.slice(pendingTransaction.ref.indexOf(':') + 1);
    return [
      {query: `/a/${symbol}/confirm/${txid}`, ...hostData}, 'rout',
      result => {
        if (result === 1 || result === true || result === 'true') {
          pendingTransaction.status = STATUS_COMPLETED;
          pendingTransaction.progress = 1;
        } else if (!isNaN(result)) {
          pendingTransaction.progress = result;
        } else {
          pendingTransaction.progress = 0;
        }
      }
    ];
  } else if (type === 'deal' && pendingTransaction.status !== STATUS_COMPLETED && (now - pendingTransaction.timestamp) < MAX_SWAP_UPDATE_THRESHOLD) {
    return [
      {query: `/e/swap/deal/status/${pendingTransaction.meta.id}`, ...hostData}, 'rout',
      result => {
        if (Number(result.progress) === 1 && result.status === 'done') {
          pendingTransaction.status = STATUS_COMPLETED;
          pendingTransaction.progress = 1;
        } else if (result.status === 'rejected' || result.status === 'timeout') pendingTransaction.status = STATUS_FAILED;
        else {
          pendingTransaction.status = STATUS_PENDING;
          pendingTransaction.progress = isNaN(result.progress) ? 0 : result.progress;
        }
        pendingTransaction.meta.ask.txid = result.ask.txid ? result.ask.txid : 
                                             pendingTransaction.meta.ask.txid ? pendingTransaction.meta.ask.txid : null;
        pendingTransaction.meta.bid.txid = result.bid.txid ? result.bid.txid : null;
      }
    ];
  } else return [];
}

function refreshPending (pendingTransactions, hasSession, data, dataCallback, errorCallback) {
  const updateSteps = {};
  // TODO maybe a type filter
  for (const ref in pendingTransactions) {
    const pendingTransaction = pendingTransactions[ref];
    updateSteps[ref] = refreshPendingTransaction(data, pendingTransaction);
  }
  return this.parallel({...updateSteps, _options: {failIfAllFail: false}}, finalize(pendingTransactions, data, dataCallback, hasSession).bind(this), errorCallback);
}

/**
 * Merge the stored pending transactions with the existing ones
 * @param  {object} storedPendingTransactions [description]
 */
const mergeStoredPending = (pendingTransactions, hasSession, data, dataCallback, errorCallback) => function (storedPendingTransactions) {
  pendingTransactions = mergeStrategy(pendingTransactions, storedPendingTransactions);
  return refreshPending.bind(this)(pendingTransactions, hasSession, data, dataCallback, errorCallback);
};

/**
 * secondary merge strategy to resolve conflicts in remote and local storage
 * @param  {[type]} local  [description]
 * @param  {[type]} remote [description]
 * @returns {[type]}        [description]
 */
function mergeStrategy (local, remote) {
  if (typeof local !== 'object' || local === null) local = {};
  if (typeof remote !== 'object' || remote === null) return local;

  // remove older entries if entryCount exceeds MAX_ENTRIES
  let values = Object.values(remote);
  if (values.length > MAX_ENTRIES) {
    let datesToDelete = values.map(el => el.timestamp).sort((a, b) => a - b);
    datesToDelete.splice(-MAX_ENTRIES);
    Object.entries(local).forEach(([ref, {
      timestamp
    }]) => {
      if (datesToDelete.includes(timestamp)) {
        if (local.hasOwnProperty(ref)) delete local[ref];
        if (remote.hasOwnProperty(ref)) delete remote[ref];
      }
    });
  }

  // sync local and remote
  for (const ref in remote) {
    if (!local.hasOwnProperty(ref) || (local.hasOwnProperty(ref) && local[ref] !== remote[ref])) local[ref] = remote[ref];
  }

  return local;
}

/**
   * Retrieve pending transactions
   * @category Transaction
   * @param {Object} data
   * @param {String} data.data - An encrypted and stringified string, array or object.
   * @param {Object} [data.refresh=true] - Whether to update the status first
   * @param {Object} [data.remove=true] - Whether to remove confirmed or finalized transaction
   * @param {Object} [data.sync=true] - Whether to synchronize the data with storage (if refreshed)
   * @example
   * hybrix.sequential([
   *   {username: 'DUMMYDUMMYDUMMY0', password: 'DUMMYDUMMYDUMMY0'}, 'session',
   *   {ref:'test', type:'regular', meta:{symbol:'dummy',id:'TX01'}}, 'addPending',
   *   'getPending'
   * ]
   *   , onSuccess
   *   , onError
   *   , onProgress
   * );
   */
exports.getPending = (pendingTransactions, fail, hasSession) => function (data, dataCallback, errorCallback) {
  if (typeof data === 'undefined') data = {};
  if (typeof data !== 'object' || data === null) return fail('Expected data to be an object', errorCallback);
  else if (data.refresh === false) return finalize(pendingTransactions, data, dataCallback, hasSession).bind(this)();
  else if (data.sync !== false && hasSession()) {
    const followUp = mergeStoredPending(pendingTransactions, hasSession, data, dataCallback, errorCallback).bind(this);
    return this.load(
      {key: PENDING_TRANSACTION_STORAGE_KEY, sessionKey: true},
      followUp,
      () => followUp({}) // if not available, use empty data
    );
  } else return refreshPending.bind(this)(pendingTransactions, hasSession, data, dataCallback, errorCallback);
};
