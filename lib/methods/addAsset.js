const initAssetWithBlob = (initData, dataCallback, errorCallback) => function (blob) {
  this.initAsset({...initData, clientModuleCodeBlob: blob, check: false}, dataCallback, errorCallback); // no need to re-check the hash
};

const loadBlobFromRemote = (mode, hostData, initData, dataCallback, errorCallback, storage, hash) => function () {
  const storageMode = mode.split('.')[0];
  this.rout({query: '/s/deterministic/code/' + storageMode, ...hostData}, blob => {
    if (hash && storage !== null) {
      storage.save({key: '_deterministic_blob_' + storageMode, value: blob, sizeOverride: true}, () => {
        storage.save({key: '_deterministic_hash_' + storageMode, value: hash}, () => {}, error => console.error('addAsset: Error saving deterministic blob hash! -> ' + error));
      }, error => console.error('addAsset: Error saving deterministic blob data! -> ' + error));
    }
    initAssetWithBlob(initData, dataCallback, errorCallback).bind(this)(blob);
  }, errorCallback);
};

const compareHashes = (mode, hostData, initData, dataCallback, errorCallback, storage) => function (hashes) {
  const storageMode = mode.split('.')[0];
  const localHash = hashes.local;
  const remoteHash = typeof hashes.remote === 'object' && hashes.remote !== null && hashes.remote.hasOwnProperty('hash') ? hashes.remote.hash : 'error';
  // DEBUG:  console.log(` >>> storageMode: ${storageMode}, localHash: ${localHash}, remoteHash: ${remoteHash}`);
  const fallbackCallback = loadBlobFromRemote(mode, hostData, initData, dataCallback, errorCallback, storage, remoteHash).bind(this);
  if (typeof hashes === 'object' && hashes !== null && remoteHash === localHash) {
    storage.load({key: '_deterministic_blob_' + storageMode}, blob => {
        initAssetWithBlob(initData, dataCallback, fallbackCallback).bind(this)(blob)
      }, fallbackCallback
    );
  } else return fallbackCallback();
};

const addBaseAsset = (data, details, dataCallback, errorCallback, storage) => function () {
  if (details['fee-symbol'] === details.symbol || data.includeBaseAssets === false) dataCallback();
  else this.addAsset({symbol: details['fee-symbol'], offset: data.offset}, dataCallback, errorCallback);
};

const handleDetails = (data, fail, storage, clientModules, hostData, dataCallback, errorCallback) => function (details) {
  // DEBUG: console.log(' >>> handle details: '+JSON.stringify(details));
  if (details.hasOwnProperty('unified-symbols') && details['unified-symbols'] !== 'null' && details['unified-symbols'] !== null) {
    return this.addUnifiedAsset({
      symbol: details.symbol,
      symbols: details['unified-symbols'],
      name: details.name,
      info: details.info,
      contract: details.contract,
      factor: details.factor,
      mode: details.mode,
      offset: data.offset,
      ...hostData
      // TODO , keys: data.keys, privateKey: data.privateKey
    }, dataCallback, errorCallback);
  } else {
    const mode = details.mode;
    const storageMode = mode.split('.')[0];
    const initData = {
      assetDetails: details,
      seed: data.seed,
      keys: data.keys,
      privateKey: data.privateKey,
      offset: data.offset,
      ...hostData
    };
    // also add base asset if fee symbol is different
    const followUp = addBaseAsset(
      data,
      details,
      () => this.asset({symbol: data.symbol, offset: data.offset}, dataCallback, errorCallback), // pass original asset data (instead of base data)
      errorCallback
    ).bind(this);

    if (clientModules.hasOwnProperty(storageMode)) { // Client Module was already retrieved
      return this.initAsset(initData, followUp, errorCallback);
    } else if (data.hasOwnProperty('clientModuleCodeBlob')) { // Use provided  clientModuleBlob
      return this.initAsset({...initData, clientModuleCodeBlob: data.clientModuleCodeBlob, check: data.check}, followUp, errorCallback);
    } else if (typeof storage === 'object' && storage !== null) { // retrieve blob from local storage and compare the remote hash
      // DEBUG: console.log(' >>> loadhash '+storageMode);
      return this.parallel(
        {
          local: {data: {func: (data, dataCallback, errorCallback) => storage.load({key: '_deterministic_hash_' + storageMode}, dataCallback, dataCallback)}, step: 'call'},
          remote: {data: {query: '/s/deterministic/hash/' + storageMode, ...hostData}, step: 'rout'}
        },
        compareHashes(mode, hostData, initData, followUp, errorCallback, storage).bind(this),
        errorCallback
      );
    } else { // fetch clientModule blob from host
      // DEBUG: console.log(' >>> loadBlob '+mode);
      return loadBlobFromRemote(mode, hostData, initData, followUp, errorCallback, null, null).bind(this)();
    }
  }
};

/**
   * Add an asset (crypto currency or token) to the session.
   * @category AssetManagement
   * @param {Object} data
   * @param {string} data.symbol - The symbol of the asset
   * @param {string} [data.seed] - CAREFUL! Using this feature incorrectly can reduce your security and privacy. Only use when you know what you're doing. Pass a custom seed to generate the asset keys
   * @param {string} [data.keys] - CAREFUL! Using this feature incorrectly can reduce your security and privacy. Only use when you know what you're doing. Pass a keypair
   * @param {string} [data.privateKey] - CAREFUL! Using this feature incorrectly can reduce your security and privacy. Only use when you know what you're doing. Pass a privateKey
   * @param {string} [data.clientModuleCodeBlob] - A string containing the client module code blob.
   * @param {Boolean} [data.check=true] - Whether to check if the provided clientModuleCodeBlob matches the remote version
   * @param {string} [data.offset=0] - The deterministic offset
   * @param {string} [data.host] - The host used for the calls.
   * @param {string} [data.channel] - The channel used for the calls. 'y' for encryped, 'z' for encryped and compresses;
   * @param {string} [data.includeBaseAssets=true] - Add the base asset too. (Main chain for token)
   * @example
   * hybrix.sequential([
   * {username: 'DUMMYDUMMYDUMMY0', password: 'DUMMYDUMMYDUMMY0'}, 'session',
   * {host: 'http://localhost:1111/'}, 'addHost',
   * {symbol: 'dummy'}, 'addAsset'
   * ]
   *   , onSuccess
   *   , onError
   *   , onProgress
   * );
   */
exports.addAsset = (assets, fail, clientModules, storage) => function (data, dataCallback, errorCallback) {
  if (typeof data === 'string') data = {symbol: data};
  else if (typeof data !== 'object' || data === null) return fail('Expected data to be an object.', errorCallback);

  if (!data.hasOwnProperty('symbol')) return fail('Missing symbol property.', errorCallback);
  if (typeof data.symbol !== 'string') return fail('Expected symbol to be a string.', errorCallback);

  const hostData = {channel: data.channel, host: data.host};
  const followUp = handleDetails(data, fail, storage, clientModules, hostData, dataCallback, errorCallback).bind(this);

  const tag = data.offset ? data.symbol + '#' + data.offset : data.symbol;
  if (!assets.hasOwnProperty(tag)) { // if assets has not been iniated, retrieve and initialize
    // asset details are cached in localForage for 24 hours
    const getDetailsFromRemote = (data, hostData, followUp, errorCallback) => {
      this.sequential([
          {query: '/a/' + data.symbol + '/details', ...hostData}, 'rout',
          (details) => {
            // DEBUG: console.log(' >>> loading details from remote');
            if (typeof details === 'object' && details) {
              result = {
                rout: {data: details, step:'id'},
                save: {data: {remote:false, encrypted:false, key:'assetDetailsCache-'+data.symbol, value:JSON.stringify(details)}, step:'save'}
              }
            } else {
              result = {
                rout: {data: null, step:'id'}
              }
            }
            return result;
          }, 'parallel',
          (results) => { return results.rout; }
        ], followUp, errorCallback);
    }
    return this.meta({remote:false, key:'assetDetailsCache-'+data.symbol},
      (meta) => {
        let modTime = 0;
        if (meta && typeof meta.local === 'object' && typeof meta.local.time !== 'undefined') {
          modTime = meta.local.time.update > meta.local.time.create?meta.local.time.update:meta.local.time.create;
        }
        const cacheMilliSeconds = 10800000;
        if (modTime > Date.now()-cacheMilliSeconds) {
          // DEBUG: console.log(' >>> loading details from local cache');
          this.load({remote:false, encrypted:false, key:'assetDetailsCache-'+data.symbol},
            (detailsString) => {
              let details = null;
              try {
                details = JSON.parse(detailsString);
              } catch (e) { console.warn(`addAsset: cache data has been corrupted for ${data.symbol}`); }
              if (typeof details === 'object' && details !== null && typeof details.mode === 'string' && typeof details.name === 'string' && typeof details.symbol === 'string') {
                followUp(details);
              } else {
                // DEBUG: console.log(' >>> loaded data mangled, getting from remote...');
                getDetailsFromRemote(data, hostData, followUp, errorCallback);
              }
            }, errorCallback);
        } else {
          // DEBUG: console.log(' >>> loading details want to get from remote');
          getDetailsFromRemote(data, hostData, followUp, errorCallback);
        }
      }, (e) => {
        // DEBUG: console.log(' >>> loading details meta not found, fallback');
        getDetailsFromRemote(data, hostData, followUp, errorCallback);
      }
    );
  } else if (data.hasOwnProperty('privateKey') || data.hasOwnProperty('keys')) { // overwrite asset with keys or privateKey
    return followUp(assets[tag]);
  } else if (typeof dataCallback === 'function') { // nothing to do, asset already initialized
    return this.asset({symbol: data.symbol, offset: data.offset}, dataCallback, errorCallback);
  } else return undefined;
};
