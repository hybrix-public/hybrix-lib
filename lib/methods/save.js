const stringifyAndEscapeQuotes = value => { // stringify, escape quotes, and encode
  if (typeof value === 'string') return value.replace(/"/g, '\\"');
  else return JSON.stringify(value).replace(/"/g, '\\"');
};

const storageSaveGraceful = async (saveObject,dataCallback,errorCallback) => {
     saveObject.storage.save(saveObject,dataCallback,
      e => {
        console.error(e);
        if (typeof dataCallback === 'function') dataCallback();
      });
  }

const saveLocal = (key, data, storage) => [ data =>
  {
    let publicKey, signature;
    if (data.autoSigned) {
      publicKey = data.autoSigned.publicKey;
      signature = data.autoSigned.signature;
    } else {
      publicKey = data.publicKey;
      signature = data.signature;
    }
    return {key, value:data.value, publicKey, signature, storage};
  }, storageSaveGraceful];

const saveRemote = (key, data) => [ data =>
  {
    let publicKey, signature;
    if (data.autoSigned) {
      publicKey = data.autoSigned.publicKey;
      signature = data.autoSigned.signature;
    } else {
      publicKey = data.publicKey;
      signature = data.signature;
    }
    const keyAndSignature = publicKey && signature ? `${key}:${publicKey}:${signature}` : key;
    // DEBUG: console.log(JSON.stringify( {host: data.host, query: '/e/storage/save/' + keyAndSignature, data: data.value, channel: data.channel, meta: data.meta, fallback:undefined} ));
    return {host: data.host, query: '/e/storage/save/' + keyAndSignature, data: data.value, channel: data.channel, meta: data.meta, fallback:undefined};
  }, 'rout'];

/**
   * Stringify and encrypt data with user keys.
   * @category Storage
   * @param {Object} data
   * @param {String} data.key - The key identifier for the data.
   * @param {Object} data.value - A string, array or object.
   * @param {Boolean} [data.sessionKey=false] - A toggle to use a sessionKey storage method.
   * @param {Boolean} [data.encrypted=true] - whether to encrypt the data with the user key.
   * @param {String} [data.work=true] - whether to perform proof of work.
   * @param {String} [data.queue=false] - whether to queue proof of work. Execute later with queue method
   * @param {String} [data.submit=true] - whether to submit proof of work.
   * @param {String} [data.host] - The host to store the data on.
   * @param {String} [data.channel=''] - Indicate the channel 'y' for encryption, 'z' for both encryption and compression.
   * @param {Boolean} [data.local=true] - Whether to use local storage if available
   * @param {Boolean} [data.remote=true] - Whether to use remote storage
   * @example
   * hybrix.sequential([
   * {username: 'DUMMYDUMMYDUMMY0', password: 'DUMMYDUMMYDUMMY0'}, 'session',
   * {host: 'http://localhost:1111/'}, 'addHost',
   * {key:'Hello', value:'World!'}, 'save',
   * {key:'Hello'}, 'load'
   * ]
   *   , onSuccess
   *   , onError
   *   , onProgress
   * );
   */
exports.save = (storage, fail, sessionKey, hasHost) => function (data, dataCallback, errorCallback, progressCallback) {
  if (typeof dataCallback !== 'function') { dataCallback = () => {} }
  if (typeof errorCallback !== 'function') { errorCallback = (e) => { console.error('save: Error callback missing! -> '+e); } }
  if (typeof data !== 'object' || data === null) return fail('save: Expected an object!', errorCallback);
  else if (!data.hasOwnProperty('key') || typeof data.key !== 'string') return fail('save: Expected property \'key\' of type string!', errorCallback);
  else if (!data.hasOwnProperty('value')) return fail('save: expected \'value\' property!', errorCallback);
  else {
    if (!hasHost() && !storage) return fail('save: No local storage available and no hosts added!', errorCallback);

    let key;
    if (data.sessionKey === true) {
       const sessionEncodedKey = sessionKey(data.key)
       if (sessionEncodedKey === null) return fail('save: Session key storage mode requires a session!', errorCallback);
       else key = encodeURIComponent(sessionEncodedKey);
    } else key = encodeURIComponent(data.key);

    const signingSteps = data.signed === true && !data.publicKey && !data.signature
      ? [ saveData => {
            return {
              value:{data:saveData,step:'id'},
              autoSigned:[
                {seed:true},'keys',
                keys => {
                  return {
                    publicKey:{data:keys.publicKey,step:'id'},
                    signature:{data:{secretKey:keys.secretKey,message:saveData},step:'sign'}
                  }
                }, 'parallel'
              ]
            }
          }, 'parallel'
        ]
      : [ saveData => { return {value:saveData,autoSigned:false,publicKey:data.publicKey,signature:data.signature} } ];
    
    const encryptionSteps = data.encrypted === false
      ? [() => data.value]
      : [{data: data.value}, 'encrypt'];

    const saveSteps = {};

    if (data.local !== false && storage) saveSteps.local = saveLocal(key, data, storage);
    if (data.remote !== false) saveSteps.remote = saveRemote(key, data);

    return this.sequential([
      ...encryptionSteps,
      ...signingSteps,
      saveSteps, 'parallel'
    ], dataCallback, errorCallback, progressCallback);
  }
};
