/**
   * Get the private key associated to a specific asset for current session.
   * @category AssetManagement
   * @param {Object} data
   * @param {string} data.symbol - The asset symbol.
   * @param {number} [data.offset=0] - The eterministic offset to use.
  * @example
   * hybrix.sequential([
   * {username: 'DUMMYDUMMYDUMMY0', password: 'DUMMYDUMMYDUMMY0'}, 'session',
   * {host: 'http://localhost:1111/'}, 'addHost',
   * {symbol: 'dummy'}, 'getPrivateKey'
   * ]
   *   , onSuccess
   *   , onError
   *   , onProgress
   * );
   */
exports.getPrivateKey = (assets, fail, hasSession) => function (data, dataCallback, errorCallback) {
  if (!hasSession()) return fail('No session available.', errorCallback);
  if (typeof data === 'string') data = {symbol: data};
  if (typeof data !== 'object' || data === null || !data.hasOwnProperty('symbol')) return fail('Expected symbol', errorCallback);
  const tag = data.offset ? data.symbol + '#' + data.offset : data.symbol;
  return this.addAsset(data, () => dataCallback(assets[tag].data.privatekey), errorCallback);
};
