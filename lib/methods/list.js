const listLocal = (storage, pattern, data) => ({data: {func: (value, dataCallback, errorCallback) => storage.list({key:pattern}, dataCallback, errorCallback)}, step: 'call'});
const listRemote = (pattern, data) => [{host: data.host, query: '/e/storage/list/' + encodeURIComponent(pattern), channel: data.channel}, 'rout'];

const handleMerge = data => function (results, dataCallback, errorCallback) {
  const localList = results.local?results.local:[];
  const remoteList = results.remote?results.remote:[];
  const resultList = Array.from(new Set(localList.concat(remoteList)));
  return dataCallback(resultList);
};

/**
   * Check which keys matching a given pattern exist in hybrixd node storage
   * @category Storage
   * @param {Object} data
   * @param {String} data.pattern - The pattern identifier for the data.
   * @param {Boolean} [data.sessionKey=false] - A toggle to use a sessionKey storage method.
   * @param {String} [data.host] - The host to store the data on.
   * @param {String} [data.channel=''] - Indicate the channel 'y' for encryption, 'z' for both encryption and compression.
   * @param {Boolean} [data.local=true] - Whether to use local storage if available
   * @param {Boolean} [data.remote=true] - Whether to use remote storage
   * @example
   * hybrix.sequential([
   * {username: 'DUMMYDUMMYDUMMY0', password: 'DUMMYDUMMYDUMMY0'}, 'session',
   * {host: 'http://localhost:1111/'}, 'addHost',
   * {pattern:'HelloWorld', value:'World!'}, 'save',
   * {pattern:'HelloUniverse', value:'Universe!'}, 'save',
   * {pattern:'Hello*'}, 'list'
   * ]
   *   , onSuccess
   *   , onError
   *   , onProgress
   * );
   */
exports.list = (storage, fail, sessionKey, hasHost) => function (data, dataCallback, errorCallback, progressCallback) {
  if (typeof dataCallback !== 'function') { dataCallback = () => {} }
  if (typeof errorCallback !== 'function') { errorCallback = (e) => { console.error('list: error callback missing! Error: '+e); } }
  if (typeof data !== 'object' || data === null) return fail('Expected an object.', errorCallback);
  else if (!data.hasOwnProperty('pattern') || typeof data.pattern !== 'string') return fail('Expected \'pattern\' property of string type.', errorCallback);
  else {
    if (!hasHost() && !storage) return fail('No local storage available and no hosts added', errorCallback);
    const pattern = data.sessionKey===true?sessionKey(data.pattern):data.pattern;
    let listSteps = {};
    if (data.local !== false && storage) listSteps.local = listLocal(storage, pattern, data);
    if (data.remote !== false) listSteps.remote = listRemote(pattern, data);

    return this.sequential([
      listSteps, 'parallel',
      {func: handleMerge(data).bind(this)}, 'call'
    ], dataCallback, errorCallback, progressCallback);
  }
};
