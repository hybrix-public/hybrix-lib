const MAX_LENGTH_GET = 220;

const burnLocal = (key, data, storage) => [ data =>
  {
    let publicKey, signature;
    if (data.autoSigned) {
      publicKey = data.autoSigned.publicKey;
      signature = data.autoSigned.signature;
    } else {
      publicKey = data.publicKey;
      signature = data.signature;
    }
    return {key, value:data.value, publicKey, signature};
  }, storage.burn ];

const burnRemote = (key, data) => [ data =>
  {
    let publicKey, signature;
    if (data.autoSigned) {
      publicKey = data.autoSigned.publicKey;
      signature = data.autoSigned.signature;
    } else {
      publicKey = data.publicKey;
      signature = data.signature;
    }
    const keyAndSignature=publicKey&&signature?`${key}:${publicKey}:${signature}`:key;
    const routCommand = {host: data.host, query: '/e/storage/burn/' + keyAndSignature, channel: data.channel, meta: data.meta};
    return routCommand;
  }, 'rout'];

/**
   * Delete a value in storage
   * @category Storage
   * @param {Object} data
   * @param {String} data.key - The key identifier for the data.
   * @param {Boolean} [data.sessionKey=false] - A toggle to use a sessionKey storage method.
   * @param {String} [data.host] - The host to store the data on.
   * @param {String} [data.channel=''] - Indicate the channel 'y' for encryption, 'z' for both encryption and compression.
   * @param {Boolean} [data.local=true] - Whether to use local storage if available
   * @param {Boolean} [data.remote=true] - Whether to use remote storage
   * @example
   * hybrix.sequential([
   * {username: 'DUMMYDUMMYDUMMY0', password: 'DUMMYDUMMYDUMMY0'}, 'session',
   * {host: 'http://localhost:1111/'}, 'addHost',
   * {key:'Hello', value:'World!'}, 'save',
   * {key:'Hello'}, 'burn'
   * ]
   *   , onSuccess
   *   , onError
   *   , onProgress
   * );
   */
exports.burn = (storage, fail, sessionKey, hasHost) => function (data, dataCallback, errorCallback, progressCallback) {
  if (typeof dataCallback !== 'function') { dataCallback = () => {} }
  if (typeof errorCallback !== 'function') { errorCallback = (e) => { console.error('burn: Error callback missing! -> '+e); } }
  if (typeof data !== 'object' || data === null) return fail('burn: Expected an object!', errorCallback);
  else if (!data.hasOwnProperty('key') || typeof data.key !== 'string') return fail('burn: Expected property \'key\' of type string!', errorCallback);
  else {
    if (data.sessionKey === true && sessionKey(data.key) === null) return fail('burn: Session key storage mode requires a session!', errorCallback);
    if (!hasHost() && !storage) return fail('burn: No local storage available and no hosts added!', errorCallback);

    const key = data.sessionKey === true ? encodeURIComponent(sessionKey(data.key)) : encodeURIComponent(data.key);

    const signingSteps = data.signed === true && !data.publicKey && !data.signature
      ? [ burnData => {
            return {
              autoSigned:[
                {seed:true},'keys',
                keys => {
                  return {
                    publicKey:{data:keys.publicKey,step:'id'},
                    signature:{data:{secretKey:keys.secretKey,message:key},step:'sign'}
                  }
                }, 'parallel'
              ]
            }
          }, 'parallel'
        ]
      : [ burnData => { return {autoSigned:false,publicKey:data.publicKey,signature:data.signature} } ];  
  
    const burnSteps = {};

    if (data.local !== false && storage) burnSteps.local = burnLocal(key, data, storage);
    if (data.remote !== false) burnSteps.remote = burnRemote(key, data);
    
    return this.sequential([
      ...signingSteps,
      burnSteps, 'parallel'
    ], dataCallback, errorCallback, progressCallback);
  }
};
