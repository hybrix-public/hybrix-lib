/**
   * Get the keys associated to a specific asset for current session. Important: handle your private keys confidentially.
   * @category AssetManagement
   * @param {Object} data
   * @param {string} data.symbol - The asset symbol.
   * @param {number} [data.offset=0] - The deterministic offset to use.
   * @example
   * hybrix.sequential([
   * {username: 'DUMMYDUMMYDUMMY0', password: 'DUMMYDUMMYDUMMY0'}, 'session',
   * {host: 'http://localhost:1111/'}, 'addHost',
   * {symbol: 'dummy'}, 'getKeys'
   * ]
   *   , onSuccess
   *   , onError
   *   , onProgress
   * );
   */
exports.getKeys = (assets, fail, hasSession) => function (data, dataCallback, errorCallback) {
  if (!hasSession()) return fail('No session available.', errorCallback);
  if (typeof data === 'string') data = {symbol: data};
  if (typeof data !== 'object' || data === null || !data.hasOwnProperty('symbol')) return fail('Expected symbol', errorCallback);
  // TODO offset
  const tag = data.offset ? data.symbol + '#' + data.offset : data.symbol;
  return this.addAsset(data, () => {
    const keys = JSON.parse(JSON.stringify(assets[tag].data.keys));
    delete keys.mode;
    dataCallback(keys);
  }, errorCallback);
};
