const seekLocal = (storage, key, data) => [
  {func: (_, dataCallback, errorCallback) => storage.seek({key}, dataCallback, errorCallback)}, 'call'
];

const seekRemote = (key, data) => [
  {host: data.host, query: '/e/storage/seek/' + key, channel: data.channel}, 'rout'
];

/**
   * Check if a value is associated with key in the hybrixd node storage
   * @category Storage
   * @param {Object} data
   * @param {String} data.key - The key identifier for the data.
   * @param {Boolean} [data.sessionKey=false] - A toggle to use a sessionKey storage method.
   * @param {String} [data.host] - The host to store the data on.
   * @param {String} [data.channel=''] - Indicate the channel 'y' for encryption, 'z' for both encryption and compression.
   * @param {Boolean} [data.local=true] - Whether to use local storage if available
   * @param {Boolean} [data.remote=true] - Whether to use remote storage
   * @example
   * hybrix.sequential([
   * {username: 'DUMMYDUMMYDUMMY0', password: 'DUMMYDUMMYDUMMY0'}, 'session',
   * {host: 'http://localhost:1111/'}, 'addHost',
   * {key:'Hello', value:'World!'}, 'save',
   * {key:'Hello'}, 'seek'
   * ]
   *   , onSuccess
   *   , onError
   *   , onProgress
   * );
   */
exports.seek = (storage, fail, sessionKey, hasHost) => function (data, dataCallback, errorCallback, progressCallback) {
  if (typeof dataCallback !== 'function') { dataCallback = () => {} }
  if (typeof errorCallback !== 'function') { errorCallback = (e) => { console.error('seek: error callback missing! Error: '+e); } }
  if (typeof data !== 'object') return fail('Expected an object.', errorCallback);

  if (data.sessionKey === true && sessionKey(data.key) === null) return fail('seek: Session key storage mode requires a session!', errorCallback);
  if (!hasHost() && !storage) return fail('No local storage available and no hosts added', errorCallback);

  const key = data.sessionKey === true ? encodeURIComponent(sessionKey(data.key)) : encodeURIComponent(data.key);

  const handleResult = results => dataCallback(results.local === true || results.remote === true);

  const steps = {};

  if (data.local !== false && storage) steps.local = seekLocal(storage, key, data);
  if (data.remote !== false) steps.remote = seekRemote(key, data);

  return this.parallel(steps, handleResult, errorCallback, progressCallback);
};
