const Decimal = require('../../common/crypto/decimal-light');
Decimal.set({ precision: 64 });
const DEFAULT_VALUATION_CACHETIME = 5 * 60 * 1000;
const rates = {}; // {[fromSymbol] : {[toSymbol]:{rate,timestamp}}
/**
   * Get the valuations rate of an array of assets.
   * @category AssetManagement
   * @param {Object} data
   * @param {string} data.fromSymbols - Array containing asset symbols.
   * @param {string} data.toSymbol - The target asset symbol.
   * @param {string} [data.cacheTime] - Cachetime in ms.
   * @param {string} [data.channel] - Indicate the channel 'y' for encryption, 'z' for both encryption and compression.
   * @param {string} [data.host] - Select a specific host, if omitted one will be chosen at random.
   * @example
   * hybrix.getValuations({fromSymbols: ['btc:12','eth:1'], toSymbol:'eth'}
   *   , onSuccess
   *   , onError
   *   , onProgress
   * );
   */
exports.getValuations = (assets, fail, hasSession) => function (data, dataCallback, errorCallback) {
  if (typeof data !== 'object' || data === null) return fail('Expected data to be an object', errorCallback);
  if (!data.hasOwnProperty('fromSymbols') || !data.fromSymbols instanceof Array) return fail('Expected fromSymbols property to be array', errorCallback);
  if (!data.hasOwnProperty('toSymbol')) return fail('Expected toSymbol property', errorCallback);
  const fromSymbols = data.fromSymbols.join(',');
  const toSymbol = data.toSymbol;
  /* TODO: filter by known rates
  } else if (rates.hasOwnProperty(fromSymbol) &&
    rates[fromSymbol].hasOwnProperty(toSymbol) &&
    rates[fromSymbol][toSymbol].timestamp < Date.now() + (data.hasOwnProperty('cacheTime') ? data.cacheTime : DEFAULT_VALUATION_CACHETIME)
  ) {
    return dataCallback(rates[fromSymbol][toSymbol].rate * data.amount);
  */
  return this.rout({query: `/e/portfolio/valuations/${fromSymbols}/${toSymbol}`, host: data.host, channel: data.channel},
    valuations => {
      const timestamp = Date.now();
      if (!rates.hasOwnProperty(toSymbol)) rates[toSymbol] = {};
      let result = {};
      for (fromSymbol in valuations) {
        const rate = valuations[fromSymbol];
        if (!rates.hasOwnProperty(fromSymbol)) rates[fromSymbol] = {};
        rates[fromSymbol][toSymbol] = {rate, timestamp};
        rates[toSymbol][fromSymbol] = {rate: 1 / rate, timestamp};
        result[fromSymbol] = rate;
      };
      return dataCallback(result);
    }, errorCallback);
};
