const compressUnifiedAddress = require('../../common/compress-unified-address.js');

/**
   * Add a unified asset TODO
   * @category AssetManagement
   * @param {Object} data
   * @param {string} data.symbol - The symbol of the asset
   * @param {number} [data.offset=0] - The deterministic offset
   * @param {string} data.name - The name of the asset
   * @param {string} data.symbols - The symbol of the asset
   * @param {string} [data.info] - The info of the asset
   * @param {string} [data.host] - The host used for the calls.
   * @param {string} [data.channel] - The channel used for the calls. 'y' for encryped, 'z' for encryped and compresses;
   * @example
   * hybrix.sequential([
   * {username: 'DUMMYDUMMYDUMMY0', password: 'DUMMYDUMMYDUMMY0'}, 'session',
   * {host: 'http://localhost:1111/'}, 'addHost',
   * {symbol: 'xmpl', symbols:{btc:1,eth:1}}, 'addUnifiedAsset',
   * {symbol: 'xmpl'}, 'refreshAsset'
   * ]
   *   , onSuccess
   *   , onError
   *   , onProgress
   * );
   */
exports.addUnifiedAsset = assets => function (data, dataCallback, errorCallback, progressCallback) {
  if (typeof data === 'object' && typeof data.symbol === 'string' && data.symbol && typeof data.symbols === 'object') {
    const steps = {};
    for (const symbol in data.symbols) {
      steps[symbol] = {data: {symbol, offset: data.offset, channel: data.channel, host: data.host}, step: 'addAsset'};
    }
    const tag = data.offset ? data.symbol + '#' + data.offset : data.symbol;
    const addUnifiedAsset = () => {
      assets[tag] = {
        symbol: data.symbol,
        symbols: data.symbols,
        name: data.name,
        info: data.info,
        fee: 0, // TODO multi asset fee
        factor: (typeof data.factor === 'undefined') ? undefined : data.factor,
        mode: (typeof data.mode === 'undefined') ? [] : data.mode,
        contract: (typeof data.contract === 'undefined') ? [] : data.contract,
        'keygen-base': {},
        'fee-symbol': data.symbol, // TODO multi asset fee
        'fee-factor': 0, // TODO multi asset fee
        data: {
          pending: [],
          balance: 'n/a',
          subBalances: {},
          baseBalances: {},
          sufficientFuel: undefined,
          subSufficientFuels: {},
          feePerSubAsset: {},
          keys: {},
          publickey: '',
          privatekey: '',
          address: '',
          seed: [],
          mode: []
        }
      };
      const asset = assets[tag];

      for (const subSymbol in data.symbols) {
        const subTag = data.offset ? subSymbol + '#' + data.offset : subSymbol;
        const weight = data.symbols[subSymbol];
        const subAsset = assets[subTag];
        const prefix = subAsset.symbol + ':';
        const comma = (asset.data.address === '' ? '' : ',');

        // TODO multi asset fee
        asset.fee = Math.max(asset.fee, weight * subAsset.fee);
        // TODO multi asset fee
        asset['fee-factor'] = Math.max(asset['fee-factor'], Number(subAsset['fee-factor'])); // get the maximal fee-factor for to enable as small as possible fees

        if (typeof data.contract === 'undefined') asset.contract.push(prefix + subAsset.contract);

        if (typeof data.mode === 'undefined') asset.mode.push(prefix + subAsset.mode);

        if (typeof data.factor === 'undefined') asset.factor = Math.min(Number(subAsset.factor), asset.factor); // get the minimal factor to ensure consistency over assets

        asset['keygen-base'][subSymbol] = subAsset['keygen-base'];
        asset.data.keys[subSymbol] = subAsset.data.keys;
        asset.data.publickey += comma + prefix + subAsset.data.publickey;
        asset.data.privatekey += comma + prefix + subAsset.data.privatekey;

        asset.data.address += comma + prefix + subAsset.data.address;
        asset.data.seed.push(prefix + subAsset.data.seed);
        asset.data.mode.push(prefix + subAsset.data.mode);
        asset.data.feePerSubAsset[subSymbol] = {[subAsset['fee-symbol']]: subAsset.fee};
      }
      asset.data.address = data.symbol + ':' + compressUnifiedAddress.encode(asset.data.address); // return compressed unified address
      // TODO compress public, private keys?
      this.asset({symbol: data.symbol, offset: data.offset}, dataCallback, errorCallback);
    };
    this.parallel(steps, addUnifiedAsset, errorCallback, progressCallback);
  } else errorCallback('Invalid data object, and/or ill-formatted data.symbol(s)! ');
};
