/**
   * Create, sign and execute a transaction e.g. push it into the network.
   * @category Transaction
   * @param {Object} data
   * @param {String} data.symbol - The symbol of the asset
   * @param {String} data.target - The target address
   * @param {Number} data.amount - The amount that should be transferred
   * @param {Boolean} [data.validate=true] - Validate target address and available funds.
   * @param {String} [data.message] - Option to add data (message, attachment, op return) to a transaction.
   * @param {Object} [data.unspent] - Manually set the unspent data
   * @param {String} [data.source=address] - Provide optional (partial) source address.
   * @param {String} [data.comparisonSymbol=hy] - Provide symbol to compare fees for multi transactions
   * @param {number} [data.offset=0] - The source offset
   * @param {Number} [data.fee] - The fee.
   * @param {Number} [data.time] - Provide an explicit time timestamp.
   * @param {String} [data.host] - The host that should be used.
   * @param {String} [data.channel]  - Indicate the channel 'y' for encryption, 'z' for both encryption and compression
   * @param {Boolean} [data.addToPending=true]  - Indicate whether to add the transaction to pending transaction list.
   * @example
   * hybrix.sequential([
   * {username: 'DUMMYDUMMYDUMMY0', password: 'DUMMYDUMMYDUMMY0'}, 'session',
   * {host: 'http://localhost:1111/'}, 'addHost',
   * {symbol: 'dummy', target: '_dummyaddress_', amount:1}, 'transaction',
   * ]
   *   , onSuccess
   *   , onError
   *   , onProgress
   * );
   */
const sha256 = require('js-sha256')
const TX_MAX_GET_LENGTH = 0;  // all transactions currently default to using POST method

exports.transaction = () => function (data, dataCallback, errorCallback, progressCallback) {
  this.sequential({
    steps: [
      data, 'rawTransaction',
      rawTx => { if(rawTx.length<TX_MAX_GET_LENGTH) { txGet = rawTx; } else { txGet = '_'; txPost = rawTx; }; return { query: `/a/${data.symbol}/push/${txGet}`, data:txPost, channel: data.channel, host: data.host }; }, 'rout',
      // DEPRECATED: rawTx => { return { query: `/a/${data.symbol}/push/${rawTx}`, channel: data.channel, host: data.host }; }, 'rout',
      txid => {
        // save description to ephemeral data storage
        if (typeof data.description === 'string' && data.description !== '') {
          const transactionIDs = txid.split(','); // split up in case of unified tx
          for (const prefixedTransactionID of transactionIDs) {
            const transactionArray = prefixedTransactionID.split(':');
            const transactionID = transactionArray.length > 1 ? transactionArray[1] : transactionArray[0];
            const hashedTransactionID = sha256.sha256(transactionID);
            this.save({key:hashedTransactionID,value:data.description,signed:true,encrypted:false}, () => {}, () => {});
            // DEBUG: console.log(JSON.stringify({key:hashedTransactionID,value:data.description,signed:true,encrypted:false}));
          }
        }
        // add to pending
        if (data.addToPending !== false) {
          this.addPending({transaction: {ref: data.symbol + ':' + txid, type: 'regular', meta: {...data, txid}}}, () => {}, () => {});
        }
        return txid;
      }
    ]
  }, dataCallback, errorCallback, progressCallback);
};
