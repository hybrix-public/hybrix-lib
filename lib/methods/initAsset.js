const CommonUtils = require('../../common/index');

function getAssetData (data, clientModules, fail, user_keys, hasSession, dataCallback, errorCallback) {
  if (data.hasOwnProperty('offset') && typeof data.offset !== 'undefined' && Number(data.offset) !== 0) {
    if (!user_keys.hasOwnProperty('username') || !user_keys.hasOwnProperty('password')) {
      return fail('No username password session available', errorCallback);
    }
    const password = user_keys.password;
    const username = user_keys.username;
    const keys = CommonUtils.generateKeys(password, username, data.offset || 0);
    user_keys = {};
    user_keys.symbol = data.symbol;
    user_keys.privateKey = data.privateKey;
    user_keys.boxPk = keys.boxPk;
    user_keys.boxSk = keys.boxSk;
    user_keys.username = username;
    user_keys.password = password;
  }

  const symbol = data.assetDetails.symbol;
  const asset = data.assetDetails;
  asset.symbol = symbol;
  asset.data = {
    balance: 'n/a',
    sufficientFuel: undefined,
    baseBalances: {},
    pending: []
  };
  const mode = data.assetDetails.mode.split('.');
  const [baseMode, subMode] = mode;

  if (!data.assetDetails.hasOwnProperty('keygen-base')) return fail('Missing \'keygen-base\' in details.', errorCallback);

  const baseSymbol = data.assetDetails['keygen-base'];

  const addressCallback = address => {
    asset.data.address = address;
    dataCallback(asset);
  };

  const keysCallback = keys => {
    asset.data.keys = keys;
    asset.data.keys.mode = subMode;
    asset.data.publickey = clientModules[baseMode].publickey(asset.data.keys);
    asset.data.privatekey = clientModules[baseMode].privatekey(asset.data.keys);

    const address = clientModules[baseMode].address(asset.data.keys, addressCallback, errorCallback);
    if (typeof address !== 'undefined') addressCallback(address); // otherwise callback will be handled
  };

  asset['fee-symbol'] = data.assetDetails['fee-symbol'] || symbol;
  asset['fee-factor'] = data.assetDetails['fee-factor'];
  let keys;
  try {
    asset.data.mode = subMode;
    if (typeof data.keys !== 'undefined') { // get keys directly from provided input
      keys = data.keys;
    } else if (typeof data.privateKey !== 'undefined') { // generate keys from provided privateKey
      if (!clientModules[baseMode].hasOwnProperty('importPrivate')) {
        return fail('Asset ' + symbol + ' does not support importing of private keys.', errorCallback);
      } else {
        data.mode = subMode;
        keys = clientModules[baseMode].importPrivate(data);
      }
    } else if (user_keys.hasOwnProperty('symbol') && user_keys.hasOwnProperty('privateKey') && baseSymbol === user_keys.symbol) {
      if (!clientModules[baseMode].hasOwnProperty('importPrivate')) {
        return fail('Asset ' + symbol + ' does not support importing of private keys.', errorCallback);
      } else {
        data.mode = subMode;
        keys = clientModules[baseMode].importPrivate({privateKey: user_keys.privateKey});
      }
    } else {
      if (!hasSession() && !data.seed) return fail('Cannot initiate asset without a session, seed or keys.', errorCallback);

      asset.data.seed = data.seed || CommonUtils.seedGenerator(user_keys, baseSymbol); // use provided seed or generate seed based on user keys
      keys = clientModules[baseMode].keys(asset.data, keysCallback, errorCallback);
    }
  } catch (e) {
    return fail(e, errorCallback);
  }
  if (typeof keys !== 'undefined') return keysCallback(keys);
}

// TODO add example
/**
   * Initialize an asset (crypto currency or token)
   * @category AssetManagement
   * @param {Object} data
   * @param {Object} data.assetDetails - Asset details as retrieved by calling `/a/$SYMBOL/details`
   * @param {string} data.clientModuleCodeBlob - A string containing the client module code blob.
   * @param {integer} [data.offset=0] - The deterministic offset
   * @param {Boolean} [data.check=true] - Whether to check if the provided clientModuleCodeBlob matches the remote version
   * @param {string} [data.seed] - CAREFUL! Using this feature incorrectly can reduce your security and privacy. Only use when you know what you're doing. Pass a custom see
   * @param {string} [data.keys] - CAREFUL! Using this feature incorrectly can reduce your security and privacy. Only use when you know what you're doing. Pass a keypair
   * @param {string} [data.privateKey] - CAREFUL! Using this feature incorrectly can reduce your security and privacy. Only use when you know what you're doing. Pass a privateKey
   * @param {string} [data.host] - The host used for the calls.
   * @param {string} [data.channel] - The channel used for the calls. 'y' for encryped, 'z' for encryped and compresses;
   **/
exports.initAsset = (user_keys, fail, assets, clientModules, hasSession) => function (data, dataCallback, errorCallback) {
  if (typeof data !== 'object' || data === null || !data.hasOwnProperty('assetDetails')) return fail('Missing \'assetDetails\'.', errorCallback);
  if (typeof data.assetDetails !== 'object' || data.assetDetails === null) return fail('Expected \'assetDetails\' to be an object.', errorCallback);

  const tag = data.offset ? data.assetDetails.symbol + '#' + data.offset : data.assetDetails.symbol;
  const followUp = asset => {
    if (!assets.hasOwnProperty(tag)) assets[tag] = asset; // prevent data race overwrite
    this.asset({symbol: asset.symbol, offset: data.offset}, dataCallback, errorCallback);
  };
  const initializeAsset = () => getAssetData(data, clientModules, fail, user_keys, hasSession, followUp, errorCallback);
  const storageMode = data.assetDetails.mode.split('.')[0];
  if (!clientModules.hasOwnProperty(storageMode)) { //  blob was not yet initialized
    return this.import({id: storageMode, blob: data.clientModuleCodeBlob, channel: data.channel, host: data.host, check: data.check}, initializeAsset, errorCallback);
  } else return initializeAsset();
};
