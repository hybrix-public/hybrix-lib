#!/bin/sh
OLDPATH=$PATH
WHEREAMI=`pwd`

# $HYBRIXD/interface/scripts/npm  => $HYBRIXD
SCRIPTDIR="`dirname \"$0\"`"
HYBRIXD="`cd \"$SCRIPTDIR/../../..\" && pwd`"

NODEJS="$HYBRIXD/nodejs"
COMMON="$HYBRIXD/common"

URL_NODEJS="https://gitlab.com/hybrix-public/dependencies/nodejs.git"
URL_COMMON="https://gitlab.com/hybrix-public/common.git"
URL_NODE="https://gitlab.com/hybrix-public/node.git"
INTERFACE="$HYBRIXD/hybrix-lib"
NODE="$HYBRIXD/node"

ENVIRONMENT="public"
echo "[i] Environment is public..."

if [ "`uname`" = "Darwin" ]; then
    SYSTEM="darwin-x64"
elif [ "`uname -m`" = "i386" ] || [ "`uname -m`" = "i686" ] || [ "`uname -m`" = "x86_64" ]; then
    SYSTEM="linux-x64"
else
    echo "[!] Unknown Architecture (or incomplete implementation)"
    export PATH="$OLDPATH"
    cd "$WHEREAMI"
    exit 1;
fi

# NODE_BINARIES
if [ ! -e "$INTERFACE/node_binaries" ];then

    echo "[!] node_binaries not found."

    if [ ! -e "$NODEJS" ];then
        cd "$HYBRIXD"
        echo "[i] Clone node js runtimes files"
        git clone "$URL_NODEJS"
    fi
    echo "[i] Link NODEJS files"
    ln -sf "$NODEJS/$SYSTEM" "$INTERFACE/node_binaries"
fi
export PATH="$INTERFACE/node_binaries/bin:$PATH"


# COMMON
if [ ! -e "$INTERFACE/common" ];then

    echo "[!] common not found."

    if [ ! -e "$COMMON" ];then
        cd "$HYBRIXD"
        echo "[i] Clone common files"
        git clone "$URL_COMMON"

    fi
    echo "[i] Link common files"
    ln -sf "$COMMON" "$INTERFACE/common"

fi

# NODE
if [ ! -e "$HYBRIXD/node" ];then
    echo "[!] node not found."
    cd "$HYBRIXD"
    echo "[i] Clone node files"
    git clone "$URL_NODE"
    ln -sf "hybrixd" "node"

    echo "[i] Run node setup"
    sh "$NODE/scripts/npm/setup.sh"
fi

# GIT HOOKS
# DEPRECATED: sh "$COMMON/hooks/hooks.sh" "$INTERFACE"

export PATH="$OLDPATH"
cd "$WHEREAMI"
