# v0.8.7
## 01-12-2021

* amended default symbols
* get balances using the portfolio engine
* missing assets are added before portfolio routing call


# v0.8.6
## 16-11-2021

* correction to valuation of HY unifieds
* ensure zero balances are not counted when sending subBalances
* allow plain channel communications for testing


# v0.8.5
## 22-10-2021

* make sure followup timing does not race ahead of remote timout
* increase followup interval when waiting for process


# v0.8.4
## 07-10-2021

* fix breaking split bug for unified raw transactions
* update dist compiles


# v0.8.3
## 07-10-2021

* bugfix pending to also record transaction IDs
* update dist compiles


# v0.8.2
## 29-09-2021

* cosmetic changes
* latest release compiled


# v0.8.1
## 17-09-2021

* pass offset to signing of transaction
* handle regressions related to HY protocol
* disable HY protocol v2.0
* amend rawTransaction calls
* make error messages more brief


# v0.8.0
## 12-08-2021

* updated latest compile
* pending now works over TOR using swap module
* ensure to pick correct feesymbol
* ensure null data does not break execution
* proper fee treatment of hy.cny, hy.eur, hy.usd
* added memoryStorage method to cache deterministic blobs
* save deterministic blobs only once per mode collection
* remove pending from results on clear
* stricter checking of status
