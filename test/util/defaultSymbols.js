const DEFAULT_TEST_SYMBOLS = [
  // 'ark', deprecated
  'bch',
  'btc',
  // 'burst', non-essential
  'dummy',
  'eth',
  // 'flo', deprecated
  // 'dash', parked
  // 'dgb', deprecated
  'etc',
  // 'lsk', parked
  'ltc',
  'nxt',
  // 'omni', non-essential
  // 'rise', non-essential
  // 'shift', non-essential
  // 'ubq', non-essential
  'tomo',
  'waves',
  // 'xcp', deprecated
  'xem',
  'xrp',
  // 'zec', parked
  // 'mock.btc', parked
  'vic.hy',
  'eth.hy',
  'bnb.hy',
  'hy'
  // 'xel', deprecated
  // 'exp', deprecated
];

exports.DEFAULT_TEST_SYMBOLS = DEFAULT_TEST_SYMBOLS;
